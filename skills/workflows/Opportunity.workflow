<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Closed_Won_Approved_Email</fullName>
        <description>Closed Won Approved Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>elisabeth.kooijmans@comodoca.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sales@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Enterprise/Closed_Won_Approved</template>
    </alerts>
    <alerts>
        <fullName>Closed_Won_Rejected</fullName>
        <description>Closed Won Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sales@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Closed_Won_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Closed_Won_Submitted_Email</fullName>
        <description>Closed Won Submitted Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sales@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Closed_Won_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_rep_with_an_opportunity_with_a_Close_Date_in_the_Past</fullName>
        <description>Email alert to rep with an opportunity with a Close Date in the past</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sales@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Close_Date_in_the_Past</template>
    </alerts>
    <alerts>
        <fullName>Email_on_Closed_Won_Opportunity</fullName>
        <description>Email on Closed Won Opportunity</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>sales@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/New_Opportunity_created</template>
    </alerts>
    <alerts>
        <fullName>Mid_Market_Deal_Approval_Alert</fullName>
        <description>Mid Market Deal Approval Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Director_Mid_Market</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>sales@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Mid_Market_Deal_Approval_Requested</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date_Update</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Close Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_Status_Updated2</fullName>
        <field>Status_Date__c</field>
        <formula>TODAY()</formula>
        <name>Current Status Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Recall</literalValue>
        <name>Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_to_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Update Approval  to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Partner_Amount</fullName>
        <field>Closed_Partner_Amount__c</field>
        <formula>Partner_Amount__c</formula>
        <name>Update Closed Partner Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_to_Closed_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Update Opportunity to Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_to_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update Opportunity to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approval_to_approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update approval to approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approval_to_rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update approval to rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Close Date Update</fullName>
        <actions>
            <name>Close_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>When the Stage is changed to Closed Won or Closed Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Date in the Past</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Send an email asking the rep to update the stage to Closed Lost or move the Close Date to the future</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_to_rep_with_an_opportunity_with_a_Close_Date_in_the_Past</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_to_rep_with_an_opportunity_with_a_Close_Date_in_the_Past</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_to_rep_with_an_opportunity_with_a_Close_Date_in_the_Past</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_to_rep_with_an_opportunity_with_a_Close_Date_in_the_Past</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_to_rep_with_an_opportunity_with_a_Close_Date_in_the_Past</name>
                <type>Alert</type>
            </actions>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_to_rep_with_an_opportunity_with_a_Close_Date_in_the_Past</name>
                <type>Alert</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Closed Partner Amount</fullName>
        <actions>
            <name>Update_Closed_Partner_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Closed_Partner_Amount__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates the closed partner amount field to the amount in the partner amount formula to lock in the partner rate upon closure so that if partner updates to new tier closed opps do not change.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Current Status Updated</fullName>
        <actions>
            <name>Current_Status_Updated2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Current_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email when Opportunity is Created</fullName>
        <actions>
            <name>Email_on_Closed_Won_Opportunity</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email notification to Account Owner &amp; Opportunity Owner when the Opportunity is Created</description>
        <formula>Owner.Id  &lt;&gt;  Account.Owner.Id</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mid Market Deal Approval Requested</fullName>
        <actions>
            <name>Mid_Market_Deal_Approval_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email alert whenever someone on the Mid Market deal moves an opportunity to deal approval stage</description>
        <formula>AND( TEXT (StageName) =&quot;Deal Approval&quot;, Owner.UserRole.Name  = &quot;Mid Market Rep&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Procebook Assignment Workflow</fullName>
        <active>false</active>
        <booleanFilter>1 AND ( 2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Deal Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update approval status on creation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Marketing Qualified Lead</value>
        </criteriaItems>
        <description>Update Approval Status to &quot;Not Submitted&quot; when opp is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Opportunity_Approval</fullName>
        <assignedTo>admin@comodoca.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Opportunity Approval</subject>
    </tasks>
</Workflow>
