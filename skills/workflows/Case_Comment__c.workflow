<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Comment_Email_Alert</fullName>
        <description>Case Comment Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>support@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Comment_Email_Template</template>
    </alerts>
    <rules>
        <fullName>EmailToCustomer</fullName>
        <actions>
            <name>Case_Comment_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to customer whenever Case Owner adds comment on Case</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
