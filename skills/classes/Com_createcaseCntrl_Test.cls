/** Class Name   : Com_createcaseCntrl_Test
*  Description  : Test class for "Com_createcaseCntrl"  
*                     
*  Created By   : Agile Cluod
*  Created On   : 27th Jan 2019

*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/

@IsTest
public class Com_createcaseCntrl_Test{
   private static testMethod void NoConsWithLeadEmail(){
       Lead le= New Lead();
       le.Casecreated__c=false;
       le.Email='test@gmail.com';
       le.LastName='test';
       Le.company='Test company';
       le.status='New';
       le.LeadSource='Chat';
       insert le;
       Com_createcaseCntrl.closeCase (le.id);
   }
   private static testmethod void oneconwithleadEmail(){
   Account acc= new Account();
   acc.Name='Test Account';
   insert acc;
      Contact con= new Contact();
      con.LastName='test con1';
      con.Email='testcon1@gmail.com';
      con.Accountid=acc.id;
      insert con;
      Case cs= new case();
      cs.Reason='Billing';
      cs.Status='New';
      cs.Origin='web';
      cs.AccountId=acc.id;
      cs.ContactId=con.id;
      insert cs;
      Lead le= new Lead();
      le.LastName='test 2 Lead';
      le.Casecreated__c=false;
      le.company='test company 1';
      le.status='New';
      le.LeadSource='Chat';
      le.Email='testcon1@gmail.com';
      insert le;
      Com_createcaseCntrl.closeCase (le.id);
   }
}