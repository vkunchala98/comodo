/*
* Purpose: Apex controller for quick action on lead to create a case with account, contact   
* Created By: Agile Cloud 26/12/2018
* Last Modified By: 
* Current Version:  v1.0 
* Revision Log:  
*              v1.1 - 
*              v1.0 -           
*/
public class Com_createcaseCntrl {
    
    /**
* Method to create standard lead conversion. Create Account,contact and case.
* @param Lead Record  
* @return Boolean
**/
    @AuraEnabled
    public static String closeCase (Id recordId ){
        try{
            //set<string> LeadEmailids= new set<string>();
            List<contact> conwithleadEmail;
            String leadEm;
            Lead myLead=[select id,status,Additional_Information__c,Street,city,State,PostalCode,Country,Email,Phone,MobilePhone,Title,Company,Name,Website,Casecreated__c from Lead where id=:recordId limit 1];
            if(myLead.Casecreated__c==false){
                if(myLead.Email!=null){
                    //LeadEmailids.add(myLead.Email);
                    leadEm=myLead.Email;
                    conwithleadEmail=[select Accountid,Email from contact where Accountid!=null AND Email=:leadEm ];                  
                }
                //If contacts are found with the same email id 
                if(conwithleadEmail.size()>0){
                    if(conwithleadEmail.size()==1){
                        system.debug('$$$$$$$$@#@single contactconwithleadEmail[0].email#@#'+conwithleadEmail[0].email);
                        String leadEmal = conwithleadEmail[0].id;
                        //Query the open cases under this contact
                        List<Case> myOpencases=[SELECT id,casenumber,Order_Number__c,contactid,SuppliedEmail,CreatedDate FROM Case WHERE contactid=: leadEmal AND Status != 'Closed' ORDER BY CreatedDate DESC limit 1];
                        system.debug('$$$$$$$$Query>>>'+myOpencases);
                        Savepoint sp = Database.setSavepoint();
                        if(!myOpencases.isempty()){
                            system.debug('$$$$$$$$@#@single open case#@#'+conwithleadEmail);
                            
                            //update the case with public case comment 
                            Case_Comment__c cc= new Case_Comment__c();
                            if(String.isNotEmpty(myLead.Additional_Information__c))
                                cc.Comment_Body__c=myLead.Additional_Information__c;
                            cc.ParentId__c=myOpencases[0].id;
                            //cc.From_Address__c=UserEmail;
                            //cc.To_Address__c='support@comodoca.com';
                            insert cc;
                            // try{
                            myLead.Casecreated__c=true;
                            mylead.Status='Qualified';
                            update myLead;
                            // }
                            // catch(exception ex){
                            //    Database.rollback(sp);
                            // }
                            return 'caseupdated';
                        }else{
                            system.debug('$$$$$$$$@#@no open case#@#'+conwithleadEmail);
                            
                            //There is no open cases under contact, create a new case
                            //Single contact get the account and create a case with contact and account
                            //get the sales Queue Id
                            Group SalesQueueId= [select Id from Group where Type = 'Queue' AND DeveloperNAME = 'Sales' limit 1];
                            //case masterrecordtypeid=[select recordtypeId from Case   Limit 1];
                            Savepoint sp1 = Database.setSavepoint();
                            //create new case with above create Account and Contact
                            Case newCase= new Case();
                            newCase.Reason='Sales';
                            newCase.AccountId=conwithleadEmail[0].Accountid;
                            newCase.ContactId=conwithleadEmail[0].id;
                            newCase.Status='New';
                            newCase.Origin='Web';
                            newCase.SuppliedEmail=conwithleadEmail[0].email;
                            newCase.OwnerId=SalesQueueId.Id;
                            insert newCase;
                            //try{
                            myLead.Casecreated__c=true;
                            mylead.Status='Qualified';
                            update myLead;
                            // }
                            // catch(exception ex){
                            //    Database.rollback(sp1);
                            // }
                            return 'singleCon';
                        }
                    }else{
                        system.debug('$$$$$$$$@#@mutiple contacts#@#'+conwithleadEmail);
                        
                        // More contacts, show an error to user that need to contact admin
                        return 'MutipleCon';
                    }
                    //This is for Found Email on contact 
                    //return 'conEmailExist' ;
                }else {
                    
                    //Need to work on rollback 
                    // Savepoint sp3 = Database.setSavepoint();
                    //No contacts 
                    //get the retailorecordtypeId from Account
                    Id AccRetailRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
                    //get the retailorecordtypeId from Contact
                    Id ConRetailRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
                    //create new Account with Lead info
                    Account acc= new Account();
                    if(myLead.Company!=null){
                        Acc.Name=myLead.Company;
                    }
                    if(	myLead.Website!=null){
                        Acc.Website=myLead.Website;
                    }
                    if(	myLead.Street!=null){
                        Acc.BillingStreet=myLead.Street;
                    }
                    if(	myLead.city!=null){
                        Acc.BillingCity=myLead.city;
                    }
                    if(	myLead.State!=null){
                        Acc.BillingState=myLead.State;
                    }
                    if(	myLead.PostalCode!=null){
                        Acc.BillingPostalCode=myLead.PostalCode;
                    }
                    if(	myLead.Phone!=null){
                        Acc.Phone=myLead.Phone;
                    }
                    if(	AccRetailRecordTypeId!=null){
                        Acc.RecordtypeId=AccRetailRecordTypeId ;
                    }
                    //insert Acc;
                    Database.DMLOptions dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.AllowSave = true; 
                    Database.SaveResult sr = Database.insert(acc, dml);
                    //create new contact with lead info
                    Contact con= new Contact();
                    if(	myLead.Title!=null){
                        con.Title=myLead.Title;
                    }
                    if(	myLead.Name!=null){
                        con.LastName=myLead.Name;
                    }
                    if(	myLead.Email!=null){
                        con.Email=myLead.Email;
                    }
                    if(	myLead.Phone!=null){
                        con.Phone=myLead.Phone;
                    }
                    if(	myLead.MobilePhone!=null){
                        con.MobilePhone=myLead.MobilePhone;
                    }
                    if(	myLead.Street!=null){
                        con.MailingStreet=myLead.Street;
                    }
                    if(	myLead.city!=null){
                        con.MailingCity=myLead.city;
                    }
                    if(	myLead.State!=null){
                        con.MailingState=myLead.State;
                    }
                    if(	myLead.PostalCode!=null){
                        con.MailingPostalCode=myLead.PostalCode;
                    }
                    if(	ConRetailRecordTypeId!=null)
                        con.RecordtypeId=ConRetailRecordTypeId ;
                    if(	sr.getId()!=null)
                        con.AccountId=sr.getId();
                    //insert con;
                    Database.DMLOptions dml2 = new Database.DMLOptions();
                    dml2.DuplicateRuleHeader.AllowSave = true; 
                    Database.SaveResult sr2 = Database.insert(con, dml2);
                    //get the sales Queue Id
                    Group SalesQueueId= [select Id from Group where Type = 'Queue' AND DeveloperNAME = 'Sales' limit 1];
                    // case masterrecordtypeid=[select recordtypeId from Case   Limit 1];
                    //create new case with above create Account and Contact
                    Case newCase= new Case();
                    if(sr.getId() !=null && sr2.getid()!=null){
                        newCase.Reason='Sales';
                        if(sr.getId() != null)
                            newCase.AccountId=sr.getId();
                        if(sr2.getId() != null)
                            newCase.ContactId=sr2.getid();
                        newCase.Status='New';
                        newCase.Origin='Web';
                        if(!conwithleadEmail.isEmpty() )
                            newCase.SuppliedEmail=conwithleadEmail[0].email;
                        if(SalesQueueId.Id != null)
                            newCase.OwnerId=SalesQueueId.Id;
                        insert newCase;
                    }
                    // try{
                    myLead.Casecreated__c=true;
                    mylead.Status='Qualified';
                    update myLead;
                    // }
                    // catch(exception ex){
                    // }
                    //return newCase.id;
                    return 'Succ';
                }
            }else{
                system.debug('$$$$$$$$$%already checkbox checked $%$%');
                return 'Alreadycase';
            }
        }   catch(DmlException ex){
            // throw new AuraHandledException('User-defined error');
            // Database.rollback(sp3);
            
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
}