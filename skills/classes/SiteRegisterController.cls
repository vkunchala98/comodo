/**
 * An apex class that creates a portal user
 */
public without sharing class SiteRegisterController {
    // PORTAL_ACCOUNT_ID is the account on which the contact will be created on and then enabled as a portal user.
    // you need to add the account owner into the role hierarchy before this will work - please see Customer Portal Setup help for more information.       
    private static Id PORTAL_ACCOUNT_ID = '001q000000tH1U3';
    
    public SiteRegisterController () {
    }

    public String username {get; set;}
    public String email {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
      
    private boolean isValidPassword() {
        return password == confirmPassword;
    }
    
    public PageReference registerUser() {
        // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            return null;
        }   
        System.debug('######In default self register page####');
        //Checking Email exists in SF
        List<contact> isExisCon = isExistingContact(email);
        system.debug('@@@@isExisCon@@@@'+isExisCon);
        if(isExisCon.size()>0){ //Exists in salesforce under contact record types
        
           //Check email is already associated with community user
           List<User> isExistUsr = isCommunityUserEmail(isExisCon[0].id);
            if(isExistUsr.size()>0){
               //Show some message , user already exists . show username
            
            }else{
            
            System.debug('$$$$$$$$');
              String profileNme = Label.Profilename;
              List<Profile> commPfid=[select Id, Name from Profile where name =:profileNme limit 1];

               //Email is not associated with Community user. Create an inactive user and notify to sales rep.
                    User u = new User();
                    u.Username = 'test'+email;
                    u.isActive=false;
                    u.lastname='test002';
                       // u.IsPortalEnabled = false;
                       u.alias='sdfsf3';
                    u.isSelfRegister__c =true;
                    u.ContactId = isExisCon[0].id;
                    u.Email = email;
                    u.profileId = commPfid[0].id;
                    u.CommunityNickname = communityNickname;
                     u.TimeZoneSidKey='America/New_York';
                u.EmailEncodingKey='ISO-8859-1';
                u.LocaleSidKey='en_US';
                u.LanguageLocaleKey='en_US';
                    
                    String accountId = PORTAL_ACCOUNT_ID;
            
                    // lastName is a required field on user, but if it isn't specified, we'll default it to the username
                    //String userId = Site.createPortalUser(u, accountId, password,false);
                    insert u;
                    if (u != null) { 
                        if (password != null && password.length() > 1) {
                            //return Site.login(,, null);
                        }
                        else {
                            PageReference page = System.Page.SiteRegisterConfirm;
                            page.setRedirect(true);
                            return page;
                        }
                    }
            
            
            }
        }else{
          // Send Rejection email
          
            //OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];
            EmailTemplate templateId = [Select id from EmailTemplate where name = 'Support Rejection Email'];
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.toAddresses = new String[] {'veera@dazeworks.com'};

            mail.setTemplateID(templateId.Id); 
            mail.setSaveAsActivity(false);
            //mail.setOrgWideEmailAddressId(owa.id);
            allmsg.add(mail);
            Messaging.sendEmail(allmsg,false);
        
        }
        return null;
    }
    //Check the Email is exists in salsforce under Channel partner and Enterprise contact Record types
    public List<contact> isExistingContact(String newEmail){
    
        Id channelPartnerId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Channel Partners').getRecordTypeId();
        Id enterpriseId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Enterprise').getRecordTypeId();
        system.debug('>>>>'+channelPartnerId+'<<<<<'+enterpriseId);
        return [SELECT Name, AccountId, RecordTypeId FROM Contact WHERE Email =:newEmail AND ( RecordTypeId =:channelPartnerId OR RecordTypeId =:enterpriseId) limit 1];
       
    }
    
    // Check Email is Community user or not 
    public List<User> isCommunityUserEmail(String conId){
    
     String prfName = Label.Profilename;
     return [SELECT Id,Name,Email FROM User WHERE Profile.Name =:prfName AND ContactId =:conId];

    }
    
    
}