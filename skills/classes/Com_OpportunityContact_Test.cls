@isTest(SeeAllData=true)
public class Com_OpportunityContact_Test {
    @isTest
    Public static  void Com_OpportunityContact_testmethod1(){
        Account a=new Account();
        a.Name='example';
        insert a;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        list<Pricebook2> EnterpricePB = new list<Pricebook2>([select Id,Name From PriceBook2 where Id =: '01s1N000007yAZu' limit 1]);
system.debug('@@@@'+EnterpricePB);        
        
        Id RecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise').getRecordTypeId();
        
        Opportunity o=new Opportunity();
        o.RecordTypeId = RecordTypeIdOpportunity;
        o.Name='Example1';
        o.StageName='Presentation';
        o.AccountId=a.Id;
        o.CloseDate=system.today();
        o.Order_Number__c =  '123456';
        o.Contract_Term_Months__c = 123;
        //o.Pricebook2Id = EnterpricePB[0].Id ;
        insert o;
        Opportunity o1=new Opportunity();
        o1.RecordTypeId = RecordTypeIdOpportunity;
        o1.Name='Example2';
        o1.StageName='Deal Approval';
        o1.AccountId=a.Id;
        o1.CloseDate=system.today();
        o1.Order_Number__c =  '12345689';
        o1.Contract_Term_Months__c = 12;
        //o1.Pricebook2Id = EnterpricePB[0].Id;
        insert o1;
        
        // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = EnterpricePB[0].Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem oppli = new OpportunityLineItem(); //---->Create OpportunityLineItem.
        oppli.PricebookEntryId=customPrice.Id;
        oppli.OpportunityId = o1.Id;
        oppli.Quantity = 5;
        oppli.UnitPrice=1200;
        insert oppli;
        test.startTest();
        try{
            o1.StageName='Closed Won';
            update o1;
            o.StageName='Sales Qualified Lead';
            update o;
            
        }
        catch(Exception e) {
            system.debug('@@@$'+e);
           // System.assert(e.getMessage().contains(System.Label.Opportunity_Sales_Qualified_Lead_error));
           // System.assert(e.getMessage().contains(System.Label.Opportunity_Close));
        }
        test.stopTest();
    }
    
}