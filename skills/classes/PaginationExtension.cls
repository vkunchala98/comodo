public with sharing class PaginationExtension {
    Public Integer noOfRecords {get;set;}
    Public Integer size {get;set;}
    Public List < SelectOption > ListOfCse {get;set;}
    Public String selectVal {get;set;}
    public ApexPages.StandardSetController setCon {get;set;}
   
    public PaginationExtension() {
     if (setCon == null) {
      size = 10;
      User u= [Select Id, Contact.AccountId,profileid from User where ID =: UserInfo.getUserId()];
      string queryString;
      String AccountID =u.Contact.AccountId;
      
    sectigoChannelPartner__c  mc1 =sectigoChannelPartner__c.getInstance(u.profileid);
    system.debug('$$$$$$$$'+mc1.IsChannelPartner__c );
    
      if (!mc1.IsChannelPartner__c) {
       queryString = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID  ORDER BY CreatedDate DESC';
       System.debug('$$$queryString $$$' + queryString);
   
      } else {
       //Incommonllc query
       //Query the Accounts that are related to incommonllc 
       Map < Id, Account > myAccInCommon = new Map < Id, Account > ([SELECT id, Channel_partner__c, Channel_partner__r.Name, Name FROM Account WHERE Channel_partner__r.id=:AccountID]);
       List < Id > myIncomm=new List<Id>();
       //= myAccInCommon.keyset();
       
       for(Id eachRec : myAccInCommon.keyset()){
       myIncomm.add(eachRec);
       }
       myIncomm.add(AccountID);
       System.debug('>>Incommonssl myIncomm onload query>>'+myIncomm);
       queryString = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId IN: myIncomm ORDER BY CreatedDate DESC';
       System.debug('$$$incommonllc$$$' + queryString);
      }
      setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
      setCon.setPageSize(size);
      noOfRecords = setCon.getResultSize();
     }
    }
   
    public void fetchRecs() {
    
          User u= [Select Id, Contact.AccountId,profileid from User where ID =: UserInfo.getUserId()];
          String AccountID=u.Contact.AccountId;
          
          sectigoChannelPartner__c  mc1 =sectigoChannelPartner__c.getInstance(u.profileid);
          system.debug('$$$$$$$$>>>'+mc1.IsChannelPartner__c );
      
    
     // String AccountID = [Select Id, Contact.AccountId from User where ID =: UserInfo.getUserId()].Contact.AccountId;
   
     String myStatus1 = 'New';
     String myStatus2 = 'In progress';
     String myStatus3 = 'Closed';
     string queryString1;
   
     if (!mc1.IsChannelPartner__c) {
        //Normal query
   
      if (selectVal == 'New') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID AND Status=:myStatus1  ORDER BY CreatedDate DESC';
      }
      if (selectVal == 'In progress') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID AND Status=:myStatus2  ORDER BY CreatedDate DESC';
      }
      if (selectVal == 'Closed') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID AND Status=:myStatus3  ORDER BY CreatedDate DESC';
      }
      if (selectVal == 'All') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID ORDER BY CreatedDate DESC';
      }
      System.debug('###queryString###' + queryString1);
      System.debug('###selectVal###' + selectVal);
   
     } else {
   
      //Incommonllc query
      //Query the Accounts that are related to incommonllc 
      Map < Id, Account > myAccInCommon1 = new Map < Id, Account > ([SELECT id, Channel_partner__c, Channel_partner__r.Name, Name FROM Account WHERE Channel_partner__r.id=:AccountID]);
      Set < Id > myIncomm1 = myAccInCommon1.keyset();
      myIncomm1.add(AccountID);
   
      if (selectVal == 'New') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId IN: myIncomm1 AND Status=:myStatus1  ORDER BY CreatedDate DESC';
      }
      if (selectVal == 'In progress') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId IN: myIncomm1 AND Status=:myStatus2  ORDER BY CreatedDate DESC';
      }
      if (selectVal == 'Closed') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId IN: myIncomm1 AND Status=:myStatus3  ORDER BY CreatedDate DESC';
      }
      if (selectVal == 'All') {
       queryString1 = 'select Id,CaseNumber,Status,Subject,Account.Name,contact.name,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId IN: myIncomm1 ORDER BY CreatedDate DESC';
      }
      System.debug('###queryString incommonllc###' + queryString1);
      System.debug('###selectVal incomonllc###' + selectVal);
   
     }
   
     setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString1));
     setCon.setPageSize(size);
     noOfRecords = setCon.getResultSize();
    }
   
    Public List < Case > getCases() {
     List < Case > caseList = new List < Case > ();
     if (setCon.getRecords() != null)
      for (Case c: (List < Case > ) setCon.getRecords())
       caseList.add(c);
     return caseList;
    }
   
    public List < SelectOption > getmyOpt() {
     List < SelectOption > myNewOpt = new List < SelectOption > ();
     myNewOpt.add(new SelectOption('All', 'All'));
     myNewOpt.add(new SelectOption('New', 'New'));
     myNewOpt.add(new SelectOption('In progress', 'In progress'));
     myNewOpt.add(new SelectOption('Closed', 'Closed'));
     return myNewOpt;
    }
   
    public pageReference refresh() {
     setCon = null;
     getCases();
     setCon.setPageNumber(1);
     return null;
    }
   
   }