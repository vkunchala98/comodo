@isTest
public class UpdateCaseCommentsWithAttachment_Test{

    private static testmethod void testscenario(){
        Account acc= new Account();
        acc.Name='testacc';
        insert acc;
        Contact con= new Contact();
        con.FirstName='abcFirst';
        con.LastName='abcLast';
        con.Email='test@abc.com';
        con.phone='1234';
        insert con;
         Case c= new Case();
         c.Reason='Customer Feedback';
         c.Status='In progress';
         c.Origin='web';
         insert c;
         Case_Comment__c cc= new Case_Comment__c();
         cc.ParentId__c=c.id;
         insert cc;
          Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        attach.parentId=cc.Id;
        insert attach;
    	List<Attachment> attachments=[select id, name from Attachment where parent.id=:cc.Id];
    	//System.assertEquals(1, attachments.size());
         Map<id,Case_Comment__c> cctestmap= new Map<id,Case_Comment__c>();
         cctestmap.put(cc.id,cc);
         Test.StartTest();
            UpdateCaseCommentsWithAttachment upa= new UpdateCaseCommentsWithAttachment(cctestmap);
            ID jobID = System.enqueueJob(upa);

         Test.StopTest();
    }
}