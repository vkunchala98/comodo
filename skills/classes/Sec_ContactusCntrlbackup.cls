public class Sec_ContactusCntrlbackup{

        public String firstName {get;set;}    
        public String lastName {get;set;}    
        public String email {get; set;} 
        public String phoneNumber {get; set;}    
        public String companyName {get; set;}    
        public String employeeCount {get; set;}    
        public String region {get; set;}    
        public String state {get; set;}    
        public String smimeQ {get; set;}  
        public String subjec {get; set;}    
        public String mess {get; set;} 
        public String hasError {get;set;} 
        public Boolean emailOptIn{get;set;}   

        
        
        public PageReference saveData(){
        
        System.debug('@@@@@'+firstName+'@@@@@@'+lastName+'@@@@@@@'+email+'@@@@@@@'+phoneNumber+'@@@@@@@'+companyName);
        System.debug('@@@@@'+employeeCount+'@@@@@@'+region+'@@@@@@@'+state+'@@@@@@@'+smimeQ+'@@@@@@@'+subjec+'@@@@@@'+mess);
         //email match with leads
        List < Task > toInsert = new List < Task > ();
        // List<Task> toInsertLead= new List<Task>();
        if (String.isNotEmpty(email)) {

            List < Lead > leads = [select id, email,Product_Interest__c,Territory__c,MobilePhone,LeadSource,Status,Company,FirstName, HubSpot_Employee_Count__c,Lastname from Lead where email =: email ORDER BY LastModifiedDate DESC];
            List < Contact > contacts = [select id, email,Accountid,firstname,lastname,Email_Opt_In__c from Contact where email =: email ORDER BY LastModifiedDate DESC];
            if (leads.size() == 0 && contacts.size() == 0) {
                //Create new lead
                //S_MIME_Quantity__c=smimeQ,
                Lead mySampleLead = new Lead(Company = companyName, State =state, HubSpot_Employee_Count__c =employeeCount, Phone=phoneNumber, Product_Interest__c = 'Private CA', Territory__c = region, LeadSource = 'Private CA Campaign', Status = 'New', FirstName = firstName, LastName = lastName, Email = email);

                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule = true;
                mySampleLead.setOptions(dmo);
                insert mySampleLead;
                String lSource = mySampleLead.LeadSource;
                Task newTask = new Task(Description = mess,
                    Priority = 'Normal',
                    Status = 'Completed',
                    Subject = 'Form Submitted on private CA website: '+ subjec,
                    IsReminderSet = true,
                    Type = 'Call/Email',
                    ReminderDateTime = System.now() + 1,
                    WhoId = mySampleLead.Id);
                toInsert.add(newTask);

                //Add them to campaign member
                List < Campaign > c = [select id, name from Campaign where name =: lSource limit 1];
                List < CampaignMember > cm = new list < CampaignMember > ();
                if (!c.isEmpty()) {
                    CampaignMember cml = new CampaignMember();
                    cml.campaignid = c[0].id;
                    cml.leadid = mySampleLead.id;
                    cm.add(cml);
                }
                if (!cm.isEmpty()) {
                    insert cm;
                }
                hasError = 'success';

            } else if (leads.size() == 1 || leads.size()>1) {
                // Update this lead
                //leads[0].FirstName = 'newfirstname';

                leads[0].Company = companyName;
                // leads[0].S_MIME_Quantity__c=smimeQ; 
                leads[0].Product_Interest__c = 'Private CA';
                leads[0].Territory__c = region;
                leads[0].Phone =phoneNumber;
                leads[0].LeadSource = 'Private CA Campaign';
                leads[0].Status = 'New';
                leads[0].State =state;
                leads[0].Company = companyName;
                leads[0].FirstName = firstName;
                leads[0].HubSpot_Employee_Count__c=employeeCount;
                leads[0].LastName = lastName;
                leads[0].Email = email;
                update leads;
                Task newTask = new Task(Description = mess,
                    Priority = 'Normal',
                    Status = 'Completed',
                    Subject = 'Form Submitted on private CA website: '+ subjec,
                    IsReminderSet = true,
                    Type = 'Call/Email',
                    ReminderDateTime = System.now() + 1,
                    WhoId = leads[0].Id);
                toInsert.add(newTask);

                hasError = 'success';

            } else if (contacts.size() == 1 || contacts.size()>1) {
                // Update this contact
                contacts[0].FirstName = firstName;
                contacts[0].Lastname = lastName;
                contacts[0].MobilePhone = phoneNumber;
                contacts[0].isPrivateCA__c=true;
                //if(emailOptIn==true)
                //contacts[0].Email_Opt_In__c=emailOptIn;
                update contacts;

                Task newTask = new Task(Description = mess,
                    Priority = 'Normal',
                    Status = 'Completed',
                    Subject = 'Form Submitted on private CA website: '+ subjec,
                    Type = 'Call/Email',
                    IsReminderSet = true,
                    ReminderDateTime = System.now() + 1,
                    WhoId = contacts[0].id);
                toInsert.add(newTask);
                
                //Notify Account Owner thorugh workflow

                hasError = 'success';

            } else{
                // Must be more than 1 contact or lead
                //update most recently changed
                System.debug('\nMore than 1 contact or lead.');
                hasError = 'morecontacts';
                              //  hasError = 'success';

            }
        }
        if (!toInsert.isEmpty()) {
            insert toInsert;
        }
        //not matched create   lead and 
        //create a task with webform name, subject and message under lead or contact
        return null;
    }

}