@isTest 
public class Sec_OppTeamMemberCntrlTest{
    
    static testMethod void testSec_OppTeamMemberCntrl(){
    
                     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
                    User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
                
                    System.runAs(u) {
                         Opportunity opp = new Opportunity();
                         // put required field
                         insert opp;
                
                         OpportunityTeamMember otm = new OpportunityTeamMember (OpportunityId = opp.id,UserId = u.id,TeamMemberRole ='Sales engineer');
                          insert otm;
                          
                          Sec_OppTeamMemberCntrl.checkOppTeamMem(opp.id);
                
                    }
        
        
        }
        
}