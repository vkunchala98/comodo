trigger Com_OpportunityContact on Opportunity (before Update) {
       
        set<id> oppids= new set<id>();
        set<Id> oppProduct = new set<Id>();
    
        Id OppRetailRecId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        
        Id OppEnterpriseRecId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise').getRecordTypeId();

    
        for(Opportunity opp:Trigger.new){
            system.debug('debug'+oppids);
            if(opp.StageName!=null && Trigger.oldMap.get(opp.id).StageName!=opp.StageName && opp.StageName=='Sales Qualified Lead'){
                oppids.add(opp.id);
            }
            // This is for Retail Record Type and enterprise record type. If stage is changed from “Deal Approval” to “Closed Won” 
            
            if(opp.StageName!=null && (opp.RecordTypeId == OppRetailRecId || opp.RecordTypeId ==OppEnterpriseRecId ) && Trigger.oldMap.get(opp.id).StageName != opp.StageName && Trigger.OldMap.get(Opp.Id).StageName=='Presentation' && opp.StageName=='POC/Pricing' && OppRetailRecId!=null){
                oppProduct.add(opp.id);
            }
            
        }
    
        Map<Id,OpportunityLineItem> oppItemMap = new Map<Id,OpportunityLineItem>();
    
        // To check atlease one product for closed won opportunities  for Retail and Enterprise Record types
        if(oppProduct.size()>0){
            List<OpportunityLineItem> allLineItems=[SELECT Description,Discount,Id,ListPrice,Name,OpportunityId,Subtotal,TotalPrice,Total_Price_F__c,Total_Price__c,UnitPrice,Volume_Discount_F__c,Volume_Discount__c FROM OpportunityLineItem WHERE OpportunityId IN: oppProduct];
            for(OpportunityLineItem eachItem : allLineItems){
                oppItemMap.put(eachItem.OpportunityId,eachItem);
            }
        }
        // This is for atleast one contact role
        List<OpportunityContactRole> oppconroles=[select id,OpportunityId,Role from OpportunityContactRole where OpportunityId IN:oppids];
        Map<Id,OpportunityContactRole> ocrMap = new Map<Id,OpportunityContactRole>();
        for(OpportunityContactRole ocr: oppconroles){
            ocrMap.put(ocr.OpportunityId, ocr);
        }
        //Now, perform our check
        for(Opportunity o: Trigger.new){
            //This is atlease one contact role
            if(o.StageName!=null && Trigger.oldMap.get(o.id).StageName!=o.StageName && o.StageName=='Sales Qualified Lead'){
                if(!ocrMap.containsKey(o.Id)){
                    system.debug('');
                    o.addError(System.Label.Opportunity_Sales_Qualified_Lead_error);
                }
            }
            //This is for atleast one product 
            if(o.StageName!=null && (o.RecordTypeId == OppRetailRecId || o.RecordTypeId == OppEnterpriseRecId) && Trigger.oldMap.get(o.id).StageName != o.StageName && Trigger.OldMap.get(o.Id).StageName=='Presentation' && o.StageName=='POC/Pricing'){
                if(!oppItemMap.containsKey(o.Id))
                    o.addError(System.Label.Opportunity_Close);
            }   
            if(Label.oppConCodeChecker == 'Yes'){
                if(o.StageName!=null && (o.RecordTypeId == OppRetailRecId || o.RecordTypeId == OppEnterpriseRecId) && Trigger.oldMap.get(o.id).StageName != o.StageName && Trigger.OldMap.get(o.Id).StageName=='Presentation' && o.StageName=='POC/Pricing'  && String.isBlank(o.Original_Interest__c) && String.isBlank(o.Pltform_Interest__c)){
                    o.addError(System.Label.Opp_Product_Interest_and_Platform_Interest);
                }  
            }
        }
}