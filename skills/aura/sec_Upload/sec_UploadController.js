({ 
    doInit: function(component, event, helper){ 
        var value = [{ 
            type: 'Case_Comment__c', 
            id: "a0Gq0000004c41c", 
            label: "Search Contact", 
        }]; 
        component.find("lookup").get("v.body")[0].set("v.values", value); 
    },
    UploadFinished : function(component, event, helper) {  
        var uploadedFiles = event.getParam("files");  
        var documentId = uploadedFiles[0].documentId;  
        var fileName = uploadedFiles[0].name;  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": "File "+fileName+" Uploaded successfully."  
        });  
        toastEvent.fire();  
        /* Open File after upload  
     $A.get('e.lightning:openFiles').fire({  
       recordIds: [documentId]  
     });*/  
    },  
})