({
    createCase : function( component, event, helper ) {
        console.log('>>>>>>>'+component.get("v.recordId"));
        var action = component.get("c.closeCase");
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        console.log('>>>>>>>'+component.get("v.recordId"));
        action.setCallback(this, function( response ){
            var state = response.getState();
            if( state == "SUCCESS" ){
                var data = response.getReturnValue();
                console.log('Return value from server'+data);
                if(data == "" || data == null){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "Unknown Error"
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                    
                }
                else if(data==="Alreadycase"){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "You can create only one case!"
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                } else if(data==="caseupdated"){   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "Case Updated with Lead description"
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                    
                } else if(data==="singleCon"){   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "Case Created for exisitng contact"
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                    
                }else if(data==="MutipleCon"){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "There are multiple contacts under lead. Please contact your Administarator!!"
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                } else if(data==="Succ"){
                    var toastEvent1 = $A.get("e.force:showToast");
                    toastEvent1.setParams({
                        "title": "Success!",
                        "type" : "success",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "Case Created Successfully!"
                    });
                    toastEvent1.fire();
                    helper.dismissQuickAction( component, event, helper );
                    
                }
            }else if( state == "ERROR" ){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message:>> " + 
                                    errors[0].message);
                    }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                } else {
                    console.log("Unknown error");
                    helper.dismissQuickAction( component, event, helper );
                }
            }
        });
        $A.enqueueAction( action );
        
    },
    dismissQuickAction : function( component, event, helper ){
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
})