({
    sendCase : function(component, event, helper) {
        var rec = component.get("v.recordId");
        console.log('>rec>>'+rec);
        var action = component.get("c.getOpenCases");
        action.setParams({
            "RecordId" : rec
        }); 
        action.setCallback(this, function( response ){
            var state = response.getState();
            if( state == "SUCCESS" ){
                var data = response.getReturnValue();
                console.log('%%%'+JSON.stringify(data));
                if(data == "" || data == null){
                    console.log('No Account id');
                }else{
                    component.set("v.openCases", data);
                    component.set("v.ThreeCases", data.slice(0,3));
                    console.log('$$$Account Id$**&&'+data[0]['AccountId']);
                    component.set("v.CurrentAccountId",data[0]['AccountId']);
                }
            }
            else if( state == "ERROR" ){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        var permission = component.get("c.getaccess");
        permission.setCallback(this, function(a) {
            console.log('--->'+a.getReturnValue().PermissionsEdit);
            console.log('--->'+a.getReturnValue().PermissionsDelete);
            component.set("v.Permissions", a.getReturnValue());  
        });
        $A.enqueueAction( permission );
        $A.enqueueAction( action );
    },
})