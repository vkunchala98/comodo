({
    goToNewCase : function(component, event, helper) {
        console.log('In controller new case >>');
        var eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
            "url": 'https://full-comodocacommunity.cs21.force.com/s/New-Case' 
        });
        eUrl.fire();
    },
    goToMyCase : function(component, event, helper) {
        console.log('In controller my cases >>');
        var eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
            "url": 'https://full-comodocacommunity.cs21.force.com/s/my-cases' 
        });
        eUrl.fire();
    },
    
})