<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>icAgentConsole__incAgentConfiguration__c</tab>
    <tab>standard-Case</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-Entitlement</tab>
    <tab>standard-ServiceContract</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Knowledge__kav</tab>
    <tab>standard-LiveChatTranscript</tab>
    <tab>Announcement__c</tab>
</CustomApplication>
