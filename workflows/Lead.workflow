<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Creation_Approved</fullName>
        <description>Account Creation Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Account_Approved_Email</template>
    </alerts>
    <alerts>
        <fullName>Alert_Marketing_Queue_of_new_lead</fullName>
        <description>Alert Marketing Queue of new lead</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Deal_Reg_Response_Email</fullName>
        <description>Deal Reg Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Partner_E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>partners@comodoca.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Deal_Reg_Form</template>
    </alerts>
    <alerts>
        <fullName>Email_web_to_lead_submitter</fullName>
        <description>Email web to lead submitter</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsWebtoLeademailresponseSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Lead_Approved_Email</fullName>
        <description>Lead Approved Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Approved_Email</template>
    </alerts>
    <alerts>
        <fullName>Lead_Rejected_Email</fullName>
        <description>Lead Rejected Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Rejected_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Account_Approval_to_Approved</fullName>
        <field>Account_Creation_Approval__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Account Approval to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Approval_to_Rejected</fullName>
        <field>Account_Creation_Approval__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Account Approval to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Approval_to_Submitted</fullName>
        <field>Account_Creation_Approval__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Account Approval to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Creation_App_to_Not_Sub</fullName>
        <field>Account_Creation_Approval__c</field>
        <literalValue>Not Submitted</literalValue>
        <name>Update Account Creation App to Not Sub</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_Opt_Out_to_True</fullName>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <name>Update Email Opt Out to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_email_opt_out_to_false</fullName>
        <field>HasOptedOutOfEmail</field>
        <literalValue>0</literalValue>
        <name>Update email opt out to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_segment_to_Enterprise_large</fullName>
        <field>Segment__c</field>
        <literalValue>Enterprise-Large</literalValue>
        <name>Update segment to Enterprise large</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_segment_to_mid_market</fullName>
        <field>Segment__c</field>
        <literalValue>Enterprise-MidMarket</literalValue>
        <name>Update segment to mid market</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_email_opt_in_to_false</fullName>
        <field>Email_Opt_In__c</field>
        <literalValue>0</literalValue>
        <name>update email opt in to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Email Response Partner Reg</fullName>
        <actions>
            <name>Alert_Marketing_Queue_of_new_lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Deal_Reg_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Asset_Name__c</field>
            <operation>equals</operation>
            <value>Deal Reg</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Creation Approval</fullName>
        <actions>
            <name>Update_Account_Creation_App_to_Not_Sub</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Webform</value>
        </criteriaItems>
        <description>When a lead is submitted with &quot;Webform&quot; source, update account creation approval to &quot;Not Submitted&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Email Opt Out to True</fullName>
        <actions>
            <name>Update_Email_Opt_Out_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>Canada,Austria,Belgium,Bulgaria,Croatia,Republic of Cyprus,Czech Republic,Denmark,Estonia,Finland,France,Germany,Greece,Hungary,Ireland,Italy,Latvia,Lithuania,Luxembourg,Malta,Netherlands,Poland,Portugal,Romania,Slovakia,Slovenia,Spain,Sweden</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>United Kingdom,UK</value>
        </criteriaItems>
        <description>On Creation for leads with GDPR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Opt in to false</fullName>
        <actions>
            <name>update_email_opt_in_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If opt out = true update opt in to false</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Optout to false</fullName>
        <actions>
            <name>Update_email_opt_out_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Email_Opt_In__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If opt in = true update opt out to false</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update segment to Enterprise large</fullName>
        <actions>
            <name>Update_segment_to_Enterprise_large</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.NumberOfEmployees</field>
            <operation>greaterThan</operation>
            <value>2999</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update segment to mid market</fullName>
        <actions>
            <name>Update_segment_to_mid_market</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.NumberOfEmployees</field>
            <operation>greaterOrEqual</operation>
            <value>500</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.NumberOfEmployees</field>
            <operation>lessThan</operation>
            <value>2999</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
