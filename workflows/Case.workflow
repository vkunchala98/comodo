<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AutoCloseEmailAlertToCustomer</fullName>
        <description>AutoCloseEmailAlertToCustomer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/AutoCloseEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>Auto_Response_when_Case_is_closed</fullName>
        <description>Support Case Closed AutoResponse Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/AutoCloseEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>Awaiting_reply_from_Customer</fullName>
        <description>Awaiting reply from Customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Awaiting_reply_for_72_business_hours</template>
    </alerts>
    <alerts>
        <fullName>Case_Auto_Response_for_Account_changes_cases</fullName>
        <description>Case Auto Response for Account changes cases</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Account_Changes/Support_Account_Changes</template>
    </alerts>
    <alerts>
        <fullName>Case_Escalation_Warning</fullName>
        <description>Case Escalation Warning</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Escalation_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>Notify_Accounting_Escalated_Queue_Owner</fullName>
        <description>Notify Accounting Escalated Queue Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Support_Case_escalation_notification_Email</template>
    </alerts>
    <alerts>
        <fullName>Notify_Support_Escalated_Queue_Owner</fullName>
        <description>Notify Support Escalated Queue Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Support_Case_escalation_notification_Email</template>
    </alerts>
    <alerts>
        <fullName>Notify_Validation_Escalated_Queue_Owner</fullName>
        <description>Notify Validation Escalated Queue Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Support_Case_escalation_notification_Email</template>
    </alerts>
    <alerts>
        <fullName>OnHoldEmailToCustomer</fullName>
        <description>OnHoldEmailToCustomer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/on_hold_escalated_to_customer</template>
    </alerts>
    <alerts>
        <fullName>SLA_Escalated</fullName>
        <description>SLA Escalated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Escalated_Email</template>
    </alerts>
    <alerts>
        <fullName>SSL_Abuse_Queue_Notification</fullName>
        <description>SSL Abuse Queue Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jay.wilson@comodoca.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nicole.wayland@comodoca.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tarek.kawach@comodoca.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SSL_Abuse_Queue_Notification</template>
    </alerts>
    <alerts>
        <fullName>SendEmailToValidationEscalatioGroup</fullName>
        <description>SendEmailToValidationEscalatioGroup</description>
        <protected>false</protected>
        <recipients>
            <recipient>Validation_lead</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Escalated_to_groups</template>
    </alerts>
    <alerts>
        <fullName>SendEmailtoAccountEscalationGroup</fullName>
        <description>SendEmailtoAccountEscalationGroup</description>
        <protected>false</protected>
        <recipients>
            <recipient>Accounting_lead</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Escalated_to_groups</template>
    </alerts>
    <alerts>
        <fullName>SendEmailtoCaseContactwhenCaseCreated</fullName>
        <description>Support New Case Created Notification Email To Contact</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Web_Auto_Response_Template</template>
    </alerts>
    <alerts>
        <fullName>SendEmailtoSupportEscalationGroup</fullName>
        <description>SendEmailtoSupportEscalationGroup</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support_lead</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Escalated_to_groups</template>
    </alerts>
    <alerts>
        <fullName>Support_Cases_Closed_after_5_days_in_Awaiting_Customer_Reply</fullName>
        <description>Support Cases Closed after 5 days in Awaiting Customer Reply</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Support_Closed_After_120_hrs_in_Awaiting_Customer_Reply</template>
    </alerts>
    <alerts>
        <fullName>Support_Notification_Email_To_Premier_Support_Queue</fullName>
        <ccEmails>6133554635@pcs.rogers.com</ccEmails>
        <description>Support Notification Email To Premier Support Queue</description>
        <protected>false</protected>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Support_Premier_Support_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Support_Notification_Email_to_Premier_Validation_Queue</fullName>
        <ccEmails>2015616477@vtext.com</ccEmails>
        <description>Support Notification Email to Premier Validation Queue</description>
        <protected>false</protected>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Support_Premier_Validation_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Web_to_case_auto_response</fullName>
        <description>Support Web to case auto response Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Web_Auto_Response_Template</template>
    </alerts>
    <alerts>
        <fullName>When_Case_on_Hold_send_Email_to_Customer</fullName>
        <description>Support Case OnHold Notification Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>extreme.atal@gmail.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/on_hold_escalated_to_customer</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Case_Owner_to_Support_Escalated</fullName>
        <description>Field update on Case Owner to change the case owner to Support Escalated Queue</description>
        <field>OwnerId</field>
        <lookupValue>Support_Escalations</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Case Owner to Support Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Reason</fullName>
        <description>Update case reason to Refunds</description>
        <field>Reason</field>
        <literalValue>Refunds</literalValue>
        <name>Update Case Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_send_email_to_customer</fullName>
        <field>Send_case_confirmation_email__c</field>
        <literalValue>1</literalValue>
        <name>Update send email to customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>change_case_owner_on_Accounting_Escalate</fullName>
        <description>Field update on Case Owner to change the case owner to Accounting Escalated Queue</description>
        <field>OwnerId</field>
        <lookupValue>Accounting_Escalated</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>change case owner on Accounting Escalate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>change_case_owner_on_Validation_Escalate</fullName>
        <description>Field update on Case Owner to change the case owner to Validation Escalated Queue</description>
        <field>OwnerId</field>
        <lookupValue>Validation_Escalated</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>change case owner on Validation Escalate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_case_owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Comodo_Refunds</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>update case owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account escalation</fullName>
        <actions>
            <name>SendEmailtoAccountEscalationGroup</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Account Changes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AutoCloseEMailWhenCaseStatusIsClosed</fullName>
        <actions>
            <name>AutoCloseEmailAlertToCustomer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When case status is closed send an AutocloseEmailTemplate to ContactEMail</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Awaiting Reply On Case</fullName>
        <actions>
            <name>Awaiting_reply_from_Customer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Customer Reply</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Reason Chnages</fullName>
        <actions>
            <name>OnHoldEmailToCustomer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Validation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Account Changes</value>
        </criteriaItems>
        <description>When agent changes case reason  to support escalation, validation escalation, or account escalation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Reason change NameCheap Cases</fullName>
        <actions>
            <name>Update_Case_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Web_Company_Name__c</field>
            <operation>contains</operation>
            <value>Namecheap</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>NC no actions</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Closed</fullName>
        <actions>
            <name>Auto_Response_when_Case_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Close_Case_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ResolutionDateline__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When Support Agent changes the Case Status Closed Send Notification Email to ContactEmail</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Closed After 5 days in Awaiting Customer Reply</fullName>
        <actions>
            <name>Support_Cases_Closed_after_5_days_in_Awaiting_Customer_Reply</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ResolutionDateline__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Close_Case_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Creation Account Changes Auto Response</fullName>
        <actions>
            <name>Case_Auto_Response_for_Account_changes_cases</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Account Changes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This is for newly created cases , case reason is Account Changes</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Creation Notification</fullName>
        <actions>
            <name>SendEmailtoCaseContactwhenCaseCreated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web,Email,Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Account Changes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When a new case is created with Web and Email ,if the case status is new sending an email to contacts email address</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Creation Notification for Chat and Phone</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Phone,Chat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When a new case is created if the case status is new sending an email to contacts email address</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_send_email_to_customer</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Support Case Creation Notification for Chat and Phone new</fullName>
        <actions>
            <name>SendEmailtoCaseContactwhenCaseCreated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a new case is created if the case status is new sending an email to contacts email address</description>
        <formula>OR(
AND( OR(ISPICKVAL( Origin , &apos;Phone&apos;) , ISPICKVAL( Origin ,&apos;Chat&apos;) ),Send_case_confirmation_email__c , ISBLANK(Ticketid__c) ,ISPICKVAL(Status,&apos;New&apos;)),
AND( OR(ISPICKVAL( Origin , &apos;Phone&apos;) , ISPICKVAL( Origin , &apos;Chat&apos;)),ISCHANGED(Send_case_confirmation_email__c),Send_case_confirmation_email__c,ISBLANK(Ticketid__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Escalation on Accounting %E2%80%93 Escalated</fullName>
        <actions>
            <name>Notify_Accounting_Escalated_Queue_Owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>change_case_owner_on_Accounting_Escalate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Accounting – Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the case reason picklist value is Accounting – Escalated then change the case owner to Accounting – Escalated Queue and send an Email notification to case owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Escalation on Support Escalated</fullName>
        <actions>
            <name>Notify_Support_Escalated_Queue_Owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_Case_Owner_to_Support_Escalated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Support Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Case Escalation on Validation Escalated</fullName>
        <actions>
            <name>Notify_Validation_Escalated_Queue_Owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>change_case_owner_on_Validation_Escalate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Validation Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the case reason pick list value is Validation Escalated  then change the case owner to Validation Escalated Queue and send an Email notification to case owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Case OnHold Notification</fullName>
        <actions>
            <name>When_Case_on_Hold_send_Email_to_Customer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Support Case SSL Abuse Queue Notification</fullName>
        <actions>
            <name>SSL_Abuse_Queue_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>SSL Abuse &amp; Malware</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This is to notify the users for SSL Abuse Queue case owner cases</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Notification to Premier Support Queue</fullName>
        <actions>
            <name>Support_Notification_Email_To_Premier_Support_Queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CaseNumber</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Premier Support Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send an email Notification to premier Support queue email that a new case is created to premier Support queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Notification to Premier Validation Queue</fullName>
        <actions>
            <name>Support_Notification_Email_to_Premier_Validation_Queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CaseNumber</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Premier Validation Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Ticketid__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send an email Notification to premier validation queue email that a new case is created to premier validation queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support Web to Case Auto Response</fullName>
        <actions>
            <name>Web_to_case_auto_response</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web,Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>When a new case is created via web if the case status is new sending an email to contacts email address</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Support escalation</fullName>
        <actions>
            <name>SendEmailtoSupportEscalationGroup</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Support</value>
        </criteriaItems>
        <description>When agent changes case reason  to support escalation send email to Support lead distribution group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Validation escalation</fullName>
        <actions>
            <name>SendEmailToValidationEscalatioGroup</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Validation</value>
        </criteriaItems>
        <description>When agent changes case reason  to validation escalation send email to ValidationLead Group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
