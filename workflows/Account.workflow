<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Account_is_Created</fullName>
        <description>New Account is Created</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Account_Created</template>
    </alerts>
    <alerts>
        <fullName>Notify_the_person_in_the_sales_operations_role_of_the_change_in_account_ownershi</fullName>
        <description>Notify the person in the sales operations role of the change in account ownership.</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contact_Follow_Up_SAMPLE_with_logo</template>
    </alerts>
    <fieldUpdates>
        <fullName>Copy_DiscoverOrg_ID</fullName>
        <description>Copy DiscoverOrg ID value to the DiscoverOrg Id field</description>
        <field>DSCORGPKG__External_DiscoverOrg_Id__c</field>
        <formula>DSCORGPKG__External_DiscoverOrg_Id__c</formula>
        <name>Copy DiscoverOrg ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_Type_to_Existing</fullName>
        <field>Type</field>
        <literalValue>Current Customer</literalValue>
        <name>Update Type to Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_segment_to_large_enterprise</fullName>
        <field>Segment__c</field>
        <literalValue>Enterprise-Large</literalValue>
        <name>Update segment to large enterprise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_segment_to_mid_market</fullName>
        <field>Segment__c</field>
        <literalValue>Enterprise-MidMarket</literalValue>
        <name>Update segment to mid market</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>DiscoverOrg ID not blank on creation</fullName>
        <actions>
            <name>Copy_DiscoverOrg_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.DSCORGPKG__DiscoverOrg_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Evaluates whether DiscoverOrg ID is not null when an Account is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Key People About Account Owner Changes</fullName>
        <actions>
            <name>Notify_the_person_in_the_sales_operations_role_of_the_change_in_account_ownershi</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify key people in the sales department when the owner of an account changes if the account’s annual revenue is greater than $1 million.</description>
        <formula>AND( ISCHANGED(OwnerId), AnnualRevenue &gt; 1000 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send an email when Account is created</fullName>
        <actions>
            <name>New_Account_is_Created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Customer Type</fullName>
        <actions>
            <name>Update_Customer_Type_to_Existing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Won_Opportunities__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Customer of Partner</value>
        </criteriaItems>
        <description>update customer type to existing customer if won opportunities is equal or greater to 1.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update segment to Large Enterprise</fullName>
        <actions>
            <name>Update_segment_to_large_enterprise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.NumberOfEmployees</field>
            <operation>greaterThan</operation>
            <value>2999</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update segment to mid market</fullName>
        <actions>
            <name>Update_segment_to_mid_market</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.NumberOfEmployees</field>
            <operation>greaterOrEqual</operation>
            <value>500</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.NumberOfEmployees</field>
            <operation>lessThan</operation>
            <value>2999</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
