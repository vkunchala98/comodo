trigger CaseOperation on Case (before insert, after insert, After update,Before Update) {
  if(System.label.TriggerActive == 'true'){
    
    if(Trigger.isAfter && Trigger.isInsert){
       // TriggerUtility.addCaseComments(Trigger.new);      
               TriggerUtility.sendAutoWorkflowEmail(Trigger.new,Trigger.oldMap,Trigger.newMap);
 
    }
    
    if(Trigger.isBefore && Trigger.isInsert){
       TriggerUtility.checkSpamEmails(trigger.new);
      TriggerUtility.SetEntitlementprocess(trigger.new); 
       // Added for Email Cases Case Reason  
       TriggerUtility.setCaseReason(trigger.new);
       TriggerUtility.setRAGField(trigger.new);
       
    }
    if(Trigger.isBefore && Trigger.Isupdate){
             TriggerUtility.setRAGField(trigger.new);

       List<case> caselist = new List<Case>();
      for(case c: trigger.new){
        if((c.AccountId != trigger.oldmap.get(c.id).AccountId)){
         caselist.add(c);        
        } 
      }
     if(caselist.size()>0){
        TriggerUtility.SetEntitlementprocess(caselist);
      }
    }
    if(Trigger.isAfter && Trigger.isUpdate){  
    
     Map<Id,case> casemap = TriggerUtility.casemilestonetobeclosed(trigger.newmap,trigger.oldmap);  
      TriggerUtility.CompleteMilestone(casemap);    
      TriggerUtility.sendAutoWorkflowEmail(Trigger.new,Trigger.oldMap,Trigger.newMap);   
    }
   }
}