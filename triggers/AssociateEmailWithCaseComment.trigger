trigger AssociateEmailWithCaseComment on EmailMessage (after insert,before delete) {  
    
    if(Trigger.isAfter && Trigger.isInsert){
        List<Case_Comment__c> caseComments = new List<Case_Comment__c>();
        List<Case> caseList = new List<Case>();
        Map<Id, Case_Comment__c> attachmentParentIdMap = new Map<Id, Case_Comment__c>();
                
        for(EmailMessage eMsg : Trigger.new){
          String subject = eMsg.subject;
            if(eMsg.ParentId!=null && subject.indexOf('ref:')!=-1 && subject.indexOf(':ref')!=-1){
              
                System.debug('In Email Message Trigger >>>'+eMsg.Incoming);
                if(eMsg.Incoming == false){
                    caseList.add(new Case(Id = eMsg.ParentId, Status='Awaiting Customer Reply'));
                }
                else{
                     caseList.add(new Case(Id = eMsg.ParentId, Status='In progress'));
                 }
                
             System.debug('In Email Message Trigger new case list >>>'+caseList);
            }
        }
               
    
        if(caseList.size()>0){
            update caseList;
        }
               
       
    }
    
    
    /**
        trigger fired on:- before delete
        purpose :- restrict users not to delete Emailmessage except System admin profile Users (line no: 48 to 54)
               
      **/
    
    
    if(trigger.isbefore && trigger.isdelete){
   // Profile SystemAdminId=[select id from Profile where Name='System Administrator' limit 1];
   // if (UserInfo.getProfileId() != SystemAdminId.Id){
       for(EmailMessage  eml:trigger.old){
          eml.adderror('You are not authorized to delete the record , please contact your system administrator');
       }
   // }
    }
}