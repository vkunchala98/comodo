/** Trigger Name   : createQualifyingQuestions 
*  Description  : Whenever lead is converted , create a new record in qualify question and capture the contact ,opportunity information
*  Created By   : Dazeworks
*  Created On   : 10-07-2018
*
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/
trigger createQualifyingQuestions on Lead (after update) {
    Map<Id,Lead> leadmap= new Map<Id,Lead>();
    for(Lead eachLead : Trigger.New){
        if(eachLead.IsConverted == true){
          leadmap.put(eachLead.id,eachLead);
          }
          }
          
          List<Qualifying_Questions__c> existingQQ = [select id,Name,Contact__c,Lead__c,Lead__r.Id,Opportunity__c from Qualifying_Questions__c where Lead__c IN:leadmap.keySet()];
         List<Qualifying_Questions__c> updateQQ= new List<Qualifying_Questions__c>();
                for(Qualifying_Questions__c eachQQ:existingQQ ){
                    
                    eachQQ.Lead__c = leadmap.get(eachQQ.Lead__r.Id).Id;
                    if(leadmap.get(eachQQ.Lead__r.Id).ConvertedContactId!=null){
                    eachQQ.Contact__c=leadmap.get(eachQQ.Lead__r.Id).ConvertedContactId;
                    }
                    if(leadmap.get(eachQQ.Lead__r.Id).ConvertedOpportunityId!=null){
                    eachQQ.Opportunity__c=leadmap.get(eachQQ.Lead__r.Id).ConvertedOpportunityId;
                    }
                    
                    updateQQ.add(eachQQ);
                }
                if(updateQQ.size()>0){
                  Update updateQQ;
                }
 }