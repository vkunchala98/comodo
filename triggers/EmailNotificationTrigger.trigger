//On addition of Case Comment by Case Owner we will send an Email to Customer
trigger EmailNotificationTrigger on Case_Comment__c(before insert){

    List<Messaging.SingleEmailMessage> listMail = new List<Messaging.SingleEmailMessage>();
    Map<Id, List<Case_Comment__c>> caseCommentMap = new Map<Id, List<Case_Comment__c>>();
    
    for(Case_Comment__c caseComment : Trigger.New){
        if(caseComment.Send_Email_to_Customer__c && caseComment.EmailMessageId__c==null){//Check we should Send email to Customer
            List<Case_Comment__c> commentSet = new List<Case_Comment__c>();
            if(caseCommentMap.containsKey(caseComment.ParentId__c)){
                commentSet = caseCommentMap.get(caseComment.ParentId__c);
            }
            commentSet.add(caseComment);
            caseCommentMap.put(caseComment.ParentId__c, commentSet);
        }
    }
    
    OrgWideEmailAddress orgWideEmail = null;
    if(String.isNotBlank(System.Label.OrgWideAddress)){
        OrgWideEmailAddress orgWideEmail = [select id, Address from OrgWideEmailAddress where Address = :System.Label.OrgWideAddress];
    }
    
    List<Case> updateCaseList = new List<Case>();
        
    for(Case caseObject : [Select Id, ContactId, Contact.email from Case where Id in : caseCommentMap.keyset() and ContactId!=null and Contact.email!=null]){
        if(caseCommentMap.containsKey(caseObject.Id) && caseCommentMap.get(caseObject.Id)!=null){
            caseObject.Status = 'Awaiting Customer Reply';
            updateCaseList.add(caseObject);
            for(Case_Comment__c caseComment : caseCommentMap.get(caseObject.Id)){
                caseComment.Contact__c = caseObject.ContactId;
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[]{caseObject.Contact.email};
                mail.setToAddresses(toAddresses);
                mail.setReplyTo(System.label.ToReply);
                mail.setSenderDisplayName('CRM Support');
                if(orgWideEmail!=null){
                    mail.setOrgWideEmailAddressId(orgWideEmail.Id);
                }
                String threadId = 'ref:_'+UserInfo.getOrganizationId()+'._'+caseObject.Id+':ref';
                threadId = Service.getThreadId(caseObject.Id);
                mail.setSubject(caseComment.Subject__c+ '['+threadId+']');
                mail.setBccSender(false);
                mail.setUseSignature(true);
                mail.setTargetObjectId(caseObject.ContactId);
                mail.setHtmlBody('<div style="display:none">*****Write After this Line*****</div>'+caseComment.Comment_Body__c);
                mail.saveAsActivity = false;
                listMail.add(mail);
            }
        }
    }
    
    if(Listmail.size()>0){
       //Messaging.sendEmail(listMail);
    }
    
    if(updateCaseList.size()>0){
        update updateCaseList;
    }
    
}