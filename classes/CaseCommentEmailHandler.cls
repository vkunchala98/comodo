global class CaseCommentEmailHandler implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String subject = email.subject;
        system.debug('$$$$$'+email.fromAddress);
        system.debug('#######'+email.headers+ 'EMail reference@@@'+email.references);
        system.debug('*********-->'+email.plaintextBody);
        if(subject.indexOf('ref:')!=-1 && subject.indexOf(':ref')!=-1){
            subject = subject.subString(subject.indexOf('ref:'), subject.indexOf(':ref'));
           // Case_Comment__c caseComment = new Case_Comment__c();
            EmailMessage myEmailMess = new EmailMessage();
           myEmailMess.ParentId= myEmailMess.RelatedToId =   Id.valueOf(Cases.getCaseIdFromEmailThreadId(subject));
            
            myEmailMess.Incoming=true;
            myEmailMess.isInboundMessage__c =true;
            myEmailMess.FromAddress = email.fromAddress;
            myEmailMess.FromName = email.fromName;
            myEmailMess.ToAddress= email.ToAddresses[0];
           
            if(email.ccAddresses!=null){
             string temp='';
            for(string s:email.ccAddresses){
               temp=(temp==null?s:temp+s);
               temp+=',';
             
             }
            myEmailMess.CcAddress = temp;
            }
          //  myEmailMess.BccAddress = email.
            myEmailMess.Subject = email.subject;
            if(String.isBlank(email.htmlBody)){
               // caseComment.Comment_Body__c = email.plaintextBody;
                myEmailMess.TextBody =email.plaintextBody;
            }else if(String.isNotBlank(email.htmlBody)){
                myEmailMess.HtmlBody=  Service.formatCaseComment(email.HtmlBody);
                
            }
        
            insert myEmailMess;
            system.debug('$$$myEmailMess$'+myEmailMess);
            Case caseInstance = new Case(Id=myEmailMess.ParentId, status='In progress');
           update caseInstance;
                       system.debug('$$$myCase$'+caseInstance);

            // Save attachments, if any
            if(email.textAttachments!=null)
                for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments){
                    Attachment attachment = new Attachment();
                    attachment.Name = tAttachment.fileName;
                    attachment.Body = Blob.valueOf(tAttachment.body);
                    attachment.ParentId = myEmailMess.Id;
                    insert attachment;
                }
            
            //Save any Binary Attachment
            if(email.binaryAttachments!=null)
                for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                    Attachment attachment = new Attachment();
                    attachment.Name = bAttachment.fileName;
                    attachment.Body = bAttachment.body;
                    attachment.ParentId = myEmailMess.Id;
                    insert attachment;
                }
            result.success = true;
        }
        return result;
    }
    
}