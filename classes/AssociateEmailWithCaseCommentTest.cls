/**
 * Test class for the AssociateEmailWithCaseCommentTest trigger.
 *
 * @see         AssociateEmailWithCaseCommentTest .cls
 *
 * @version     2018-14-06 Veera     first version
 *              
 */
@isTest 
public class AssociateEmailWithCaseCommentTest {

    static testMethod void testEmailMessage() { 
        Case cse=new Case(Subject= 'In Service Billable - A29'); 
        insert cse; 
        
        EmailMessage newEmail = new EmailMessage(FromAddress = 'test@abc.org', Incoming = false , ToAddress= 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com', Subject = 'Test email',HtmlBody='<html><body>Hi veera ,Changes have been made to your details. <br><br>Contact administrator if you are not responisble.</body></html>', TextBody = '23456 ', ParentId = cse.Id); 
        insert newEmail;
                
        Attachment attach=new Attachment(); 
        attach.Name = 'Unit Test Attachment'; 
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body'); 
        attach.body = bodyBlob; 
        attach.parentId = newEmail.id;
        insert attach;
        /*if(newEmail.id!=null){
        Case_Comment__c caseComment = [select Id, EmailMessageId__c, ParentId__c from Case_Comment__c where EmailMessageId__c= :newEmail.Id];
        if(caseComment!=null){
        Map<Id, Case_Comment__c> attachmentParentIdMap = new Map<Id, Case_Comment__c>();
        attachmentParentIdMap.put(newEmail.Id, caseComment);
        
        
        UpdateCaseCommentsWithAttachment attachmentsJob = new UpdateCaseCommentsWithAttachment(attachmentParentIdMap);
        System.enqueueJob(attachmentsJob);
        }
        }*/
    }
      static testmethod void testdelete(){
      String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);
          System.runAs(u) {
        Case cse=new Case(Subject= 'In Service Billable - A29'); 
        insert cse; 
        
        EmailMessage newEmail = new EmailMessage(FromAddress = 'test@abc.org', Incoming = True, ToAddress= 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com', Subject = 'Test email',HtmlBody='<html><body>Hi veera ,Changes have been made to your details. <br><br>Contact administrator if you are not responisble.</body></html>', TextBody = '23456 ', ParentId = cse.Id); 
        insert newEmail;
        
        try{
          delete newEmail;
        }catch(Exception e){
           system.assert(e.getMessage().contains('You are not authorized to delete the record'),'You are not authorized to delete the record');   

        }
        }
          
      }

}