@isTest
public class Community_Web2CaseCtrlTest {
    public static testMethod void testScenario(){
       Account acc=new Account();
    acc.Name='testacc';
    insert acc;
        Contact con=new Contact();
        con.FirstName='testfname';
        con.LastName='testlname';
        con.Email='test@abc.com';
        con.Phone='1234';
        insert con;
        Case cases = new Case();
        cases.contactId=con.id;
        cases.AccountId=acc.id;
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cases);
        Community_Web2CaseCtrl CW = new Community_Web2CaseCtrl(sc);
        CW.ReasonforTicket='Billing';
        CW.ReasonforTicket1='Billing';
        CW.uploadBtn=false;
        CW.emailIdC='test1211@tgd.com';
        CW.WebCompanyName = acc.Name;
        CW.OrderNumber = '123';
        CW.Subjects = 'sasa';
       // CW.UserId='ur.Id';
        CW.firstNameC='test1';
        CW.lastNameC='test2';
        CW.ReasonforTicket1='Billing';
        CW.emailIdC = 'test@testing.com';
        CW.WebCompanyName = 'Test Name';
        CW.OrderNumber = '123';
        CW.Subjects = 'sasa';
        CW.phoneNoC = '9786567989';
        CW.Descriptions = 'testing my value';
        //con.Email=CW.emailIdC;
        
       // CW.saveData();
        CW.fileSize();
        CW.caseOrdr();
        
        Case cse=new Case();
        insert cse;
 
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cse.id;
        insert attach;
        
        Attachment attach1=new Attachment();     
        attach1.Name='Unit Test Attachment';
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob1;
        
        CW.attachment = attach1;
        CW.uploadMore();
        CW.attachmentIdToRemove = attach.Id;
        
        Attachment attach2=new Attachment();     
        attach2.Name='Unit Test Attachment';
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body');
        attach2.body=bodyBlob2;
        
        CW.hasError = 'Go';
        CW.dmlerror=false;
        CW.attachment = attach2;
        CW.removeAttachment();
        CW.upload();
        CW.getRootCategoryName();
       CW.getCategoryGroupName();
        CW.fetchKnowledgeArticles();
         CW.fetchMostRecentArticles();
       String Emailid='test@abc.com';
      // CW.getContact(Emailid);
        PageReference myVfPage = Page.Community_WebtoCase;
       Test.setCurrentPage(myVfPage);
       ApexPages.currentPage().getParameters().put('subjectValue',CW.Subjects);
       
      Account portalAccount = new Account(name = 'portalAccount');//create a portal account first
  insert portalAccount;
  Contact portalContact = new contact(LastName = 'portalContact', AccountId = portalAccount.Id,Email='srivalli@dazeworks.com',phone='1234'); //create a portal contact
  insert portalContact;
          Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Login User'];
          Account ac = new Account(name ='Grazitti') ;
        insert ac; 
        Contact con1 = new Contact(LastName ='testCon',AccountId = ac.Id,Email='testcon1234@gmail.com',Phone='1234');
        insert con1;  
         User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United States',IsActive =true,
                ContactId = con1.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert user;

  /*User u1 = new User( email='genelia.dsouza@gmail.com',
                profileid = p.id, 
                UserName='genelia.dsouza@gmail.com', 
                Alias = 'GDS',
                TimeZoneSidKey='America/New_York',
                EmailEncodingKey='ISO-8859-1',
                LocaleSidKey='en_US', 
                LanguageLocaleKey='en_US',
                ContactId = portalContact.Id,
                PortalRole = 'Manager',
                FirstName = 'Genelia',
                LastName = 'Dsouza');*/

        
            system.runAs(user){
               CW.saveData();
            }
       
        
    }
}