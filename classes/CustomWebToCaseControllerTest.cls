/** Class Name   : CustomWebToCaseControllerTest
*  Description  : Test class for "CustomWebToCaseController"  
*                     
*  Created By   : 
*  Created On   : 

*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/
@isTest
public class CustomWebToCaseControllerTest{
    public static TestMethod void CustomWebToCaseControllerTest(){
    Account acc=new Account();
    acc.Name='testacc';
    insert acc;
        Contact con=new Contact();
        con.FirstName='testfname';
        con.LastName='testlname';
        con.Email='test@abc.com';
        con.Phone='1234';
        insert con;
        
          

        Case cases = new Case();
        cases.contactId=con.id;
        cases.AccountId=acc.id;
        CustomWebToCaseController customWeb = new CustomWebToCaseController(new ApexPages.StandardController(cases));
        customWeb.firstNameC='test1';
        customWeb.lastNameC='test2';
        customWeb.ReasonforTicket1='Billing';
        customWeb.emailIdC = 'test@testing.com';
        customWeb.WebCompanyName = 'Test Name';
        customWeb.OrderNumber = '123';
        customWeb.Subjects = 'sasa';
        customWeb.phoneNoC = '9786567989';
        customWeb.Descriptions = 'testing my value';
        con.Email=customWeb.emailIdC;
        
       
        
        customWeb.saveData();
        customWeb.fileSize();
        
        Case cse=new Case();
        insert cse;
        //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cse.id;
        insert attach;
        
        Attachment attach1=new Attachment();     
        attach1.Name='Unit Test Attachment';
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob1;
        
        customWeb.attachment = attach1;
        customWeb.uploadMore();
        customWeb.attachmentIdToRemove = attach.Id;
        
        Attachment attach2=new Attachment();     
        attach2.Name='Unit Test Attachment';
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body');
        attach2.body=bodyBlob2;
        
        customWeb.hasError = 'Go';
        customWeb.attachment = attach2;
        customWeb.removeAttachment();
        customWeb.upload();
        customWeb.getRootCategoryName();
        customWeb.getCategoryGroupName();
        customWeb.fetchKnowledgeArticles();
       String Emailid='test@abc.com';
       customWeb.getContact(Emailid);
        PageReference myVfPage = Page.Com_KnowledgeWeb2Casepage;
       Test.setCurrentPage(myVfPage);
       ApexPages.currentPage().getParameters().put('subjectValue',customWeb.Subjects);
       // ApexPages.Message[] messages = ApexPages.getMessages();

    //System.assertEquals(2, messages.size());
    //System.assertEquals('Message 1', messages[0].getDetail());
    //System.assertEquals('Message 2', messages[1].getDetail());
       // System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
        
        
        
    }
    
   public static TestMethod void saveDataElse(){
    Account acc=new Account();
    acc.Name='testacc';
    insert acc;
        Contact con=new Contact();
        con.FirstName='testfname';
        con.LastName='testlname';
        con.Email='test@abc.com';
        con.Phone='1234';
        insert con;
        
           Case c1= new Case();
        c1.contactId=con.id;
       // c1.lastName='test2';
       // customWeb.ReasonforTicket1='Billing';
        //c1.emailIdC = 'test@testing.com';
        c1.SuppliedName = 'Test Name';
        c1.Order_Number__c = '12345678';
        c1.Subject = 'sasa';
        c1.SuppliedPhone= '9786567989';
        c1.Description = 'testing my value';
        insert c1;
        
       
        Case cases = new Case();
        //cases.contactId=con.id;
        //cases.AccountId=acc.id;
        cases.Status='New';
        //cases.Order_Number__c = '12345678';

        //Insert cases;
        
        CustomWebToCaseController customWeb = new CustomWebToCaseController(new ApexPages.StandardController(cases));
                Case temp=customWeb.getMatchedOrder('12345678');

        customWeb.caseObject = cases;
        customWeb.firstNameC='test1';
        customWeb.lastNameC='test2';
        customWeb.ReasonforTicket1='Billing';
        customWeb.emailIdC = 'test@testing.com';
        customWeb.WebCompanyName = 'Test Name';
        customWeb.OrderNumber = '12345678';
        customWeb.Subjects = 'sasa';
        customWeb.phoneNoC = '9786567989';
        customWeb.Descriptions = 'testing my value';
        con.Email=customWeb.emailIdC;
        system.debug('this is savedata start');
        customWeb.saveData();
        system.debug('this is after savedata');
        customWeb.fileSize();
        
        Case cse=new Case();
        insert cse;
        //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
 
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cse.id;
        insert attach;
        
        Attachment attach1=new Attachment();     
        attach1.Name='Unit Test Attachment';
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob1;
        
        customWeb.attachment = attach1;
        customWeb.uploadMore();
        customWeb.attachmentIdToRemove = attach.Id;
       
        Attachment attach2=new Attachment();     
        attach2.Name='';
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body');
        attach2.body=bodyBlob2;
        
        customWeb.hasError = 'Go';
        customWeb.attachment = attach2;
        customWeb.removeAttachment();
        customWeb.upload();
        customWeb.getRootCategoryName();
        customWeb.getCategoryGroupName();
       
       String Emailid='test@abc.com';
       customWeb.getContact(Emailid);
        PageReference myVfPage = Page.Com_KnowledgeWeb2Casepage;
       Test.setCurrentPage(myVfPage);
       ApexPages.currentPage().getParameters().put('subjectValue',customWeb.Subjects);
       // ApexPages.Message[] messages = ApexPages.getMessages();
      /*d knowledgeRecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
        Knowledge__kav knowledgeKav = new Knowledge__kav();
        knowledgeKav.RecordTypeID=knowledgeRecordTypeId;
        knowledgeKav.Summary_Data__c='test';
        knowledgeKav.Title='sasa';
        knowledgeKav.UrlName='Test-on-21-Nov-2018';
        //knowledgeKav.ArticleTotalViewCount=23;
        knowledgeKav.Article_Number__c=3412;
        insert knowledgeKav;
        //system.debug('this is knowledgeKav >>>>>>><<<<<<<<<<<'+knowledgeKav);
       //system.debug('@@@@@@@@#######knowledgeArticleQueryresult->'+[SELECT Id,Title,urlName,ArticleTotalViewCount,Article_Number__c,Summary_Data__c FROM Knowledge__kav]);
        test.startTest();
       customWeb.fetchMostRecentArticles();       
       customWeb.fetchKnowledgeArticles();
       test.stopTest();*/
   }
    
    public static TestMethod void fetchMostRecentArticlesTest(){
         
        Account acc=new Account();
    acc.Name='testacc';
    insert acc;
        Contact con=new Contact();
        con.FirstName='testfname';
        con.LastName='testlname';
        con.Email='test@abc.com';
        con.Phone='1234';
        insert con;
        Case cases = new Case();
        //cases.contactId=con.id;
        //cases.AccountId=acc.id;
        cases.Status='New';
        //Insert cases;
        Case c1= new Case();
        c1.contactId=con.id;
       // c1.lastName='test2';
       // customWeb.ReasonforTicket1='Billing';
        //c1.emailIdC = 'test@testing.com';
        c1.SuppliedName = 'Test Name';
        c1.Order_Number__c = '12345678';
        c1.Subject = 'sasa';
        c1.SuppliedPhone= '9786567989';
        c1.Description = 'testing my value';
        insert c1;
        
        CustomWebToCaseController customWeb = new CustomWebToCaseController(new ApexPages.StandardController(cases));
        customWeb.caseObject = cases;
        customWeb.firstNameC='test1';
        customWeb.lastNameC='test2';
        customWeb.ReasonforTicket1='Billing';
        customWeb.emailIdC = 'test@testing.com';
        customWeb.WebCompanyName = 'Test Name';
        customWeb.OrderNumber = '12345678';
        customWeb.Subjects = 'sasa';
        customWeb.phoneNoC = '9786567989';
        customWeb.Descriptions = 'testing my value';
        con.Email=customWeb.emailIdC;
        
        Case cse=new Case();
        insert cse;
        String Emailid='test@abc.com';
      List<Contact> cons= customWeb.getContact(Emailid); 
        PageReference myVfPage = Page.Com_KnowledgeWeb2Casepage;
       Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('subjectValue',customWeb.Subjects);
       // ApexPages.Message[] messages = ApexPages.getMessages();
       Id knowledgeRecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
        Knowledge__kav knowledgeKav = new Knowledge__kav();
        knowledgeKav.RecordTypeID=knowledgeRecordTypeId;
        knowledgeKav.Summary_Data__c='test';
        knowledgeKav.Title='sasa';
        knowledgeKav.UrlName='Test-on-21-Nov-2018';
        //knowledgeKav.ArticleTotalViewCount=23;
        knowledgeKav.Article_Number__c=3412;
        insert knowledgeKav;
        system.debug('this is knowledgeKav >>>>>>>>><<<<<<<<<<< '+knowledgeKav);
       system.debug('@@@@@@@@#######knowledgeArticleQueryresult-> '+[SELECT Id,Title,urlName,ArticleTotalViewCount,Article_Number__c,Summary_Data__c FROM Knowledge__kav]);
        test.startTest();
       customWeb.fetchMostRecentArticles();       
       customWeb.fetchKnowledgeArticles();
       test.stopTest();
    }
}