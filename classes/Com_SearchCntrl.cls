public with sharing class Com_SearchCntrl {
    @AuraEnabled
    public static sobjectwrapperclass fetchSobjectInfo(String searchStr){
        system.debug('>>'+searchStr);
        sobjectwrapperclass myWrap= new sobjectwrapperclass();
        List<Case> caseList= new List<Case>();
        List<SObject> myKnowArt = new List<SObject>();
        if(searchStr.length() > 1){
            String searchStr1 = '*'+searchStr+'*';
            String searchQuery = 'FIND \'' + searchStr1 + '\' IN ALL FIELDS RETURNING  Case(Id,CaseNumber,Subject,Status, Order_Number__c,CreatedDate), KnowledgeArticleVersion (Id, Title,UrlName WHERE PublishStatus=\'online\')';
            List<List <sObject>> searchList = search.query(searchQuery);
            system.debug('>>sosl searchList in ctrl>>>'+searchList);
            caseList = ((List<Case>)searchList[0]);
            myKnowArt= ((List<Sobject>)searchList[1]);
            myWrap.cases=caseList;
            myWrap.knowArtles=myKnowArt;
            myWrap.caseCount=caseList.size();
            myWrap.knowCount=myKnowArt.size();
        }else{
            myWrap=null;
        }
       
        system.debug('>>Final Wrap>>'+myWrap);
        return myWrap;
    }
}