@isTest(seeAllData=true)
public class OpportunityContactRoleTest{
   private static testmethod void positivemethod(){
   Account acc= new Account();
   acc.Name='testacc';
   insert acc;
   Contact con= new Contact();
          con.FirstName='test1';
          con.LastName='test2';
          con.Accountid=acc.id;
          con.Email='test@abc.com';
          insert con;
     Opportunity opp= new Opportunity();
     opp.Name='Testopp';
     opp.StageName='Marketing Qualified Lead'; 
     opp.CloseDate=Date.Today();
     opp.Accountid=acc.id;
     opp.Amount=200;
     opp.Type='New';
     opp.DM_Identified__c=true;
     insert opp;
     
     Pricebook2 pb22 = new Pricebook2(Name='testDIE');
     insert pb22;
    
    Product2 pro2 = new Product2(Name='BXCD',isActive=true);
    insert pro2;
           
        PricebookEntry pbe2 =new PricebookEntry(unitprice=0.01,Product2Id=pro2.Id,Pricebook2Id=Test.getStandardPricebookId(),
                                                 isActive=true,UseStandardPrice = false);
         insert pbe2;
         
           // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        Pricebook2 mypbk=[select id, name from Pricebook2 where name='Enterprise Pricebook' limit 1];
        
         PricebookEntry pbe3 =new PricebookEntry(unitprice=0.01,Product2Id=pro2.Id,Pricebook2Id=mypbk.id,
                                                 isActive=true,UseStandardPrice = false);
         insert pbe3;
         
         OpportunityLineItem OPplineitem2 = new OpportunityLineItem (Quantity=2, OpportunityId=opp.Id,UnitPrice=0.01,PriceBookEntryId=pbe3.id);
          insert OPplineitem2;
          
         OpportunityContactRole occ= new OpportunityContactRole ();
         occ.Contactid=con.id;
         occ.Opportunityid=opp.id;
         occ.Role = 'Decision Maker';
         insert occ;
         opp.StageName='Sales Qualified Lead';
         update opp;
          
           
   }
}