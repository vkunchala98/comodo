public with sharing class PaginationExtension {
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    Public List<SelectOption> ListOfCse {get;set;}
    Public String selectVal {get;set;}
    public ApexPages.StandardSetController setCon{get;set;}
    
    public PaginationExtension() {
            if(setCon == null){
                size = 10;
                String AccountID = [Select Id,Contact.AccountId from User where ID=: UserInfo.getUserId()].Contact.AccountId;
                string queryString = 'select Id,CaseNumber,Status,Subject,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID  ORDER BY CreatedDate DESC';
                System.debug('$$$queryString $$$'+queryString );
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
    }
    
    public void fetchRecs(){
        String AccountID = [Select Id,Contact.AccountId from User where ID=: UserInfo.getUserId()].Contact.AccountId;
        
        String myStatus1='New';
        String myStatus2='In progress';
        String myStatus3='Closed';
        string queryString1 ;
        if(selectVal=='New'){
          queryString1 = 'select Id,CaseNumber,Status,Subject,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID AND Status=:myStatus1  ORDER BY CreatedDate DESC';
        }
        if(selectVal=='In progress'){
          queryString1 = 'select Id,CaseNumber,Status,Subject,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID AND Status=:myStatus2  ORDER BY CreatedDate DESC';
        }
        if(selectVal=='Closed'){
           queryString1 = 'select Id,CaseNumber,Status,Subject,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID AND Status=:myStatus3  ORDER BY CreatedDate DESC';
        }
        if(selectVal=='All'){
          queryString1 = 'select Id,CaseNumber,Status,Subject,Order_Number__c,Description,CreatedDate,LastModifiedDate  from Case where AccountId =:AccountID ORDER BY CreatedDate DESC';
        }
        System.debug('###queryString###'+queryString1);
                System.debug('###selectVal###'+selectVal);

        setCon=new ApexPages.StandardSetController(Database.getQueryLocator(queryString1));
        setCon.setPageSize(size);
        noOfRecords = setCon.getResultSize();
    }

    Public List<Case> getCases(){
        List<Case> caseList = new List<Case>();
        if(setCon.getRecords()!= null)
        for(Case c : (List<Case>)setCon.getRecords())
         caseList.add(c);
        return caseList;
    }
    
    public List<SelectOption> getmyOpt(){
        List<SelectOption>  myNewOpt = new List<SelectOption>();
        myNewOpt.add(new SelectOption('All','All'));
        myNewOpt.add(new SelectOption('New','New'));
        myNewOpt.add(new SelectOption('In progress','In progress'));
        myNewOpt.add(new SelectOption('Closed','Closed'));
        return myNewOpt;
    }

    public pageReference refresh() {
        setCon = null;
        getCases();
        setCon.setPageNumber(1);
        return null;
    }
       
}