@isTest
private class createQualifyingQuestionsTest {

 static testMethod void testleadconvert() {
        
        Lead testLead = new Lead();
        testLead.FirstName = 'Test First';
        testLead.LastName = 'Test Last';
        testLead.Company = 'Test Co';
        testLead.email='testlead@gmail.com';
        testLead.Account_Creation_Approval__c='Approved';
        testLead.Segment__c='Retail';
        testLead.Product_Interest__c='SMIME';
        insert testLead;
        Qualifying_Questions__c qc= new Qualifying_Questions__c();
        qc.Lead__c=testLead.id;
        insert qc;
        test.StartTest();
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(testLead.id);
            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);        
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            System.assert(lcr.isSuccess());        
        test.StopTest();
 
 
 
 }
 
 }