/**
 * Comodo Community Self Register 
 *
 * @see         
 *
 * @version     2018-10-23 Dazeworks      first version
 *              
 */ 

global without sharing class LightningSelfRegisterController {
    
    private static Id PORTAL_ACCOUNT_ID = '001q000000tH1U3';
    
    public LightningSelfRegisterController() {
        
    }
    
    @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
        return password == confirmPassword;
    }
    
    @TestVisible 
    private static boolean siteAsContainerEnabled(String communityUrl) {
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(communityUrl,'');
        return authConfig.isCommunityUsingSiteAsContainer();
    }
    
    @TestVisible 
    private static void validatePassword(User u, String password, String confirmPassword) {
        if(!Test.isRunningTest()) {
            Site.validatePassword(u, password, confirmPassword);
        }
        return;
    }
    
    @AuraEnabled
    public static String selfRegister(String firstname ,String lastname, String email, String password, String confirmPassword, String accountId, String regConfirmUrl, String extraFields, String startUrl, Boolean includePassword) {
        Savepoint sp = null;
        try {
            sp = Database.setSavepoint();
            if (lastname == null || String.isEmpty(lastname)) {
                return Label.Site.lastname_is_required;
            }
            if (email == null || String.isEmpty(email)) {
                return Label.Site.email_is_required;
            }
            System.debug('######In default self register page####');
            //Checking Email exists in SF
            List<contact> isExisCon = isExistingContact(email);
            system.debug('@@@@isExisCon@@@@'+isExisCon);
            if(isExisCon.size()>0){ //Exists in salesforce under contact record types
                
                //Check email is already associated with community user
                List<User> isExistUsr = isCommunityUserEmail(isExisCon[0].id);
                if(isExistUsr.size()>0){
                    //Show some message , user already exists . show username
                    
                    return 'This email is existing user, our support team will contact you soon!';
                    
                }else{
                    
                    System.debug('$$$$$$$$');
                    String profileNme = Label.Profilename;
                    List<Profile> commPfid=[select Id, Name from Profile where name =:profileNme limit 1];
                    User u = new User();
                    u.Username = String.valueOf(Crypto.getRandomInteger()).substring(1,5)+ email;
                    u.isActive=false;
                    u.FirstName = firstname;
                    u.lastname=lastname;
                    // u.IsPortalEnabled = false;
                    String nickname = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
                    nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                    u.CommunityNickname = nickname;
                    u.alias='sdfsf3';
                    u.isSelfRegister__c =true;
                    u.ContactId = isExisCon[0].id;
                    u.Email = email;
                    u.profileId = commPfid[0].id;
                    u.TimeZoneSidKey='America/New_York';
                    u.EmailEncodingKey='ISO-8859-1';
                    u.LocaleSidKey='en_US';
                    u.LanguageLocaleKey='en_US';
                    String accountId1 = PORTAL_ACCOUNT_ID;
                    insert u;
                    if (u != null) { 
                        if (password != null && password.length() > 1) {
                            ApexPages.PageReference lgn = Site.login(email, password, startUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(lgn);
                            }
                        }
                        else {
                            ApexPages.PageReference confirmRef = new PageReference(regConfirmUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(confirmRef);
                            }
                            
                        }
                    }
                    return null;
                    
                }
            }else{
                // Send Rejection email
                
                System.debug('In Rejection Part>>');
               // Pick a dummy Contact
                    Contact c = [select id, Email from Contact where email <> null limit 1];
                    
                    // Construct the list of emails we want to send
                    List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
                    
                    Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                    msg.setTemplateId( [select id from EmailTemplate where DeveloperName='Support_Rejection_Email'].id );
                    msg.setWhatId( [select id from Account limit 1].id );
                    msg.setTargetObjectId(c.id);
                    msg.setToAddresses(new List<String>{'srivalli@dazeworks.com'});
                    
                    lstMsgs.add(msg);
                    
                    // Send the emails in a transaction, then roll it back
                    Savepoint sp2 = Database.setSavepoint();
                    Messaging.sendEmail(lstMsgs);
                    Database.rollback(sp2);
                    
                    // For each SingleEmailMessage that was just populated by the sendEmail() method, copy its
                    // contents to a new SingleEmailMessage. Then send those new messages.
                    List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
                    for (Messaging.SingleEmailMessage email2 : lstMsgs) {
                    Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                    emailToSend.setToAddresses(email2.getToAddresses());
                    emailToSend.setPlainTextBody(email2.getPlainTextBody());
                    emailToSend.setHTMLBody(email2.getHTMLBody());
                    emailToSend.setSubject(email2.getSubject());
                    lstMsgsToSend.add(emailToSend);
                    }
                    Messaging.sendEmail(lstMsgsToSend);
            }
            return null;  
        }
        catch (Exception ex) {
            Database.rollback(sp);
            return ex.getMessage();            
        }
    }
    
    @AuraEnabled
    public static List<Map<String,Object>> getExtraFields(String extraFieldsFieldSet) { 
        List<Map<String,Object>> extraFields = new List<Map<String,Object>>();
        Schema.FieldSet fieldSet = Schema.SObjectType.User.fieldSets.getMap().get(extraFieldsFieldSet);
        if(!Test.isRunningTest()) {
            if (fieldSet != null) {
                for (Schema.FieldSetMember f : fieldSet.getFields()) {
                    Map<String, Object> fieldDetail = new Map<String, Object>();
                    fieldDetail.put('dbRequired', f.getDBRequired());
                    fieldDetail.put('fieldPath', f.getFieldPath());
                    fieldDetail.put('label', f.getLabel());
                    fieldDetail.put('required', f.getRequired());
                    fieldDetail.put('type', f.getType());
                    fieldDetail.put('value', '');   // client will populate
                    extraFields.add(fieldDetail);
                }}}
        return extraFields;
    }
    
    @AuraEnabled
    global static String setExperienceId(String expId) {
        // Return null if there is no error, else it will return the error message 
        try {
            if (expId != null) {
                Site.setExperienceId(expId);   
            }
            return null; 
        } catch (Exception ex) {
            return ex.getMessage();            
        }        
    }  
    //Check the Email is exists in salsforce under Channel partner and Enterprise contact Record types
    public static List<contact> isExistingContact(String newEmail){
        
        Id channelPartnerId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Channel Partners').getRecordTypeId();
        Id enterpriseId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Enterprise').getRecordTypeId();
        system.debug('>>>>'+channelPartnerId+'<<<<<'+enterpriseId);
        return [SELECT Name, AccountId, RecordTypeId FROM Contact WHERE Email =:newEmail AND ( RecordTypeId =:channelPartnerId OR RecordTypeId =:enterpriseId) limit 1];
        
    }
    
    // Check Email is Community user or not 
    public static List<User> isCommunityUserEmail(String conId){
        
        String prfName = Label.Profilename;
        return [SELECT Id,Name,Email FROM User WHERE Profile.Name =:prfName AND ContactId =:conId];
        
    }
}