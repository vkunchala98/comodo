/** Class Name   : CustomWebToCaseControllerTest
*  Description  : Test class for "CustomWebToCaseController"  
*                     
*  Created By   : 
*  Created On   : 

*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/
@isTest
public class CustomWebToCaseControllerTest1{
    public static TestMethod void CustomWebToCaseControllerTest1(){
    Account acc=new Account();
    acc.Name='testacc';
    insert acc;
        Contact con=new Contact();
        con.FirstName='testfname';
        con.LastName='testlname';
        con.Email='test@abc.com';
        con.Phone='1234';
        insert con;
        Case cases = new Case();
        cases.contactId=con.id;
        cases.AccountId=acc.id;
        CustomWebToCaseController1 customWeb = new CustomWebToCaseController1(new ApexPages.StandardController(cases));
        customWeb.firstNameC='test1';
        customWeb.lastNameC='test2';
        customWeb.ReasonforTicket1='Billing';
        customWeb.emailIdC = 'test@testing.com';
        customWeb.WebCompanyName = 'Test Name';
        customWeb.OrderNumber = '123';
        customWeb.Subjects = 'sasa';
        customWeb.phoneNoC = '9786567989';
        customWeb.Descriptions = 'testing my value';
        con.Email=customWeb.emailIdC;
        
        customWeb.saveData();
        customWeb.fileSize();
        
        Case cse=new Case();
        insert cse;
        //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
 
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cse.id;
        insert attach;
        
        Attachment attach1=new Attachment();     
        attach1.Name='Unit Test Attachment';
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob1;
        
        customWeb.attachment = attach1;
        customWeb.uploadMore();
        customWeb.attachmentIdToRemove = attach.Id;
        
        Attachment attach2=new Attachment();     
        attach2.Name='Unit Test Attachment';
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body');
        attach2.body=bodyBlob2;
        
        customWeb.hasError = 'Go';
        customWeb.attachment = attach2;
        customWeb.removeAttachment();
        customWeb.upload();
        customWeb.getRootCategoryName();
        customWeb.getCategoryGroupName();
        customWeb.fetchKnowledgeArticles();
        customWeb.cleardata();
       String Emailid='test@abc.com';
       customWeb.getContact(Emailid);
        PageReference myVfPage = Page.Com_KnowledgeWeb2CasepageSectigo;
       Test.setCurrentPage(myVfPage);
       ApexPages.currentPage().getParameters().put('subjectValue',customWeb.Subjects);
       // ApexPages.Message[] messages = ApexPages.getMessages();

    //System.assertEquals(2, messages.size());
    //System.assertEquals('Message 1', messages[0].getDetail());
    //System.assertEquals('Message 2', messages[1].getDetail());
       // System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
        
        
        
    }
    
    public static TestMethod void CustomWebToCaseControllerTest2(){
    
    SLA_Setting__c slaSettings = new SLA_Setting__c();
        slaSettings.Followup_SLA_Time_for_Enterprise_Support__c = 30;
        slaSettings.Initial_SLA_Time_for_Enterprise_Support__c = 30;
        slaSettings.Followup_SLA_Enterprise_Premier_CP__c = 30;
        slaSettings.Followup_SLA_Enterprise_Premier_HP__c = 30;
        slaSettings.Followup_SLA_Enterprise_Premier_LP__c = 30;
        slaSettings.Followup_SLA_Enterprise_Premier_MP__c = 30;
        slaSettings.Followup_SLA_Time_for_Support__c = 30;
        slaSettings.Initial_SLA_Time_for_Support__c = 30;
        slaSettings.Initial_SLA_Enterprise_Premier_CP__c = 30;
        slaSettings.Initial_SLA_Enterprise_Premier_HP__c = 30;
        slaSettings.Initial_SLA_Enterprise_Premier_LP__c = 30;
        slaSettings.Initial_SLA_Enterprise_Premier_MP__c = 30;
        slaSettings.Followup_SLA_Time_for_Partner_Support__c = 30;
        slaSettings.Initial_SLA_Time_for_Partner_Support__c = 30;
        slaSettings.Retail_Support_Followup_SLA__c = 30;
        slaSettings.Retail_Support_Intial_SLA__c = 30;
        slaSettings.Name='TIME';
        insert slaSettings;
        
        mileStoneCaseReason__c mileStonCaseRea = new mileStoneCaseReason__c();
        mileStonCaseRea.Name='Brand Validation';
        insert mileStonCaseRea ;
        
         mileStoneCaseReason__c mileStonCaseRea1 = new mileStoneCaseReason__c();
        mileStonCaseRea1.Name='CCM Validation';
        insert mileStonCaseRea1 ;
        
         mileStoneCaseReason__c mileStonCaseRea2 = new mileStoneCaseReason__c();
        mileStonCaseRea2.Name='EV Validation';
        insert mileStonCaseRea2 ;
    
        Account acc = new Account(
            Name = 'TestAccount',
            Segment__c = 'Partner/Reseller'    
        );
        insert acc; 
        Account acc1 = new Account(
            Name = 'TestAccount1',
            Segment__c = 'Retail',
            Enterprise_Premier__c = true    
        );
        insert acc1; 
        ServiceContract sc = new ServiceContract();  
        sc.Name = 'Partner/Reseller Support'; 
        sc.accountId = acc.Id;     
        insert sc;
        ServiceContract sc1 = new ServiceContract();  
        sc1.Name = 'Enterprise Premier Support'; 
        sc1.accountId = acc1.Id;     
        insert sc1;
        Entitlement ent = new Entitlement (
            Name = 'Service SLA',
            Type = 'Phone Support',
            AccountId = acc.Id,
            ServiceContractId = sc.Id
        );
        insert ent;
        Entitlement ent1 = new Entitlement (
            Name = 'Service SLA',
            Type = 'Phone Support',
            AccountId = acc1.Id,
            ServiceContractId = sc1.Id
        );
        insert ent1;
        contact ct = new contact(
        LastName = 'test contact',
        Email = 'vinay@dazeworks.com',
        Phone = '1234567890',
        accountId = acc.Id
        );
        insert ct;
        contact ct1 = new contact(
        LastName = 'test contact1',
        Email = 'vinay@dazework.com',
        Phone = '123456789',
        accountId = acc1.Id
        );
        insert ct1;
        Case cas1 = new Case(      
            Status = 'New',
            priority = 'High',
            Subject = 'TestCase',
            AccountId = acc.Id,
            contactId = ct.Id,
            Order_Number__c='8787787',
            EntitlementId = ent.Id      
        );     
        insert cas1;
        
   /* Account acc2=new Account();
    acc2.Name='testacc';
    insert acc2; */
     /*   Contact con=new Contact();
        con.FirstName='testfname';
        con.LastName='testlname';
        con.Email='test@abc.com';
        con.Phone='1234';
        insert con; */
       /* Case cases1 = new Case();
        cases1.contactId=con.id;
        cases1.AccountId=acc.id;
        cases1.Order_Number__c='12345';
        insert cases1; */
        
        Case cases = new Case();
        cases.contactId=ct.id;
        cases.AccountId=acc.id;
        cases.Order_Number__c='12345';
        CustomWebToCaseController1 customWeb = new CustomWebToCaseController1(new ApexPages.StandardController(cases));
        customWeb.firstNameC='test1';
        customWeb.lastNameC='test2';
        customWeb.ReasonforTicket1='Billing';
        customWeb.emailIdC = 'test@testing.com';
        customWeb.WebCompanyName = 'Test Name';
        customWeb.OrderNumber = '123';
        customWeb.Subjects = 'sasa';
        customWeb.phoneNoC = '9786567989';
        customWeb.Descriptions = 'testing my value';
        customWeb.ReasonforTicket='Test';
        ct.Email=customWeb.emailIdC;
        
        customWeb.saveData();
        customWeb.fileSize();
        
        Case cse=new Case();
        insert cse;
        //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
 
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cse.id;
        insert attach;
        
        Attachment attach1=new Attachment();     
        attach1.Name='Unit Test Attachment';
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob1;
        
        customWeb.attachment = attach1;
        customWeb.uploadMore();
        customWeb.attachmentIdToRemove = attach.Id;
        
        Attachment attach2=new Attachment();     
        attach2.Name='Unit Test Attachment';
        Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body');
        attach2.body=bodyBlob2;
        
        customWeb.hasError = 'Go';
        customWeb.attachment = attach2;
        customWeb.removeAttachment();
        customWeb.upload();
        customWeb.getRootCategoryName();
        customWeb.getCategoryGroupName();
        customWeb.fetchKnowledgeArticles();
        customWeb.getMatchedOrder('8787787');
        customWeb.cleardata();
       String Emailid='test@abc.com';
       customWeb.getContact(Emailid);
        PageReference myVfPage = Page.Com_KnowledgeWeb2CasepageSectigo;
       Test.setCurrentPage(myVfPage);
       ApexPages.currentPage().getParameters().put('subjectValue',customWeb.Subjects);
       // ApexPages.Message[] messages = ApexPages.getMessages();

    //System.assertEquals(2, messages.size());
    //System.assertEquals('Message 1', messages[0].getDetail());
    //System.assertEquals('Message 2', messages[1].getDetail());
       // System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
        
        
        
    }
}